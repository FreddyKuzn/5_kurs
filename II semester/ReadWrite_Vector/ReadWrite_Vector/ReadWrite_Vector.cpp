﻿#include <iostream>
#include <algorithm>
#include <fstream>
#include <iterator>
#include <vector>
#include <string>

using namespace std;

string filename_double("savefile_double");
string filename_int("savefile_int");

vector<double> myVector_double{ 1321.32132,16.32131,32.321,64,3213.454 };
vector<int> myVector_int{ 0,1,2,3,4,5,6,7,8,9 };

template<typename T>
void write_vector_to_file(const vector<T>& myVector, string& filename)
{
	ofstream ofs(filename, ios::out | ofstream::binary);
	ostream_iterator<char> osi{ ofs };
	const char* beginByte = (char*)&myVector[0];

	const char* endByte = (char*)&myVector.back() + sizeof(T);
	copy(beginByte, endByte, osi);
}

template<typename T>
void read_vector_from_file(vector<T>& newVector, string& filename)
{
	newVector.clear();
	vector<char> buffer{};
	ifstream ifs(filename, ios::in | ifstream::binary);
	istreambuf_iterator<char> iter(ifs);
	istreambuf_iterator<char> end{};
	copy(iter, end, back_inserter(buffer));
	newVector.resize(buffer.size() / sizeof(T));
	memcpy(&newVector[0], &buffer[0], buffer.size());
}
int main()
{
	write_vector_to_file(myVector_double, filename_double);
	vector<double> newVector_double;
	read_vector_from_file(newVector_double, filename_double);
	//printing output
	for (auto i : newVector_double)
	{
		cout << i << endl;
	}
	cout  << endl;
	///////////////////////////////////////////////////////////////
	write_vector_to_file(myVector_int, filename_int);
	vector<int> newVector_int;
	read_vector_from_file(newVector_int, filename_int);
	//printing output
	for (auto i : newVector_int)
	{
		cout << i << endl;
	}
	return 0;
}

