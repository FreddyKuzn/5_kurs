﻿
// SignalPropagationDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "SignalPropagation.h"
#include "SignalPropagationDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CSignalPropagationDlg



CSignalPropagationDlg::CSignalPropagationDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SIGNALPROPAGATION_DIALOG, pParent)
	, N_size(20) //размер сетки 20x20
	, d_size(0.25) //расстояие между узлами
	, M_size(300) //размер для исследования 300x300
	, lymda(1) //длина волны
	, R(1000) // радиус полусферы
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSignalPropagationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, N_size);
	DDX_Text(pDX, IDC_EDIT2, d_size);
	DDX_Control(pDX, IDC_ANTENNA, gdiObjLocation);
	DDX_Control(pDX, IDC_SIGNALS2D, gdiObjSignals);
	DDX_Control(pDX, IDC_CD1, viewer);
	DDX_Text(pDX, IDC_EDIT3, M_size);
	DDX_Text(pDX, IDC_EDIT4, lymda);
	DDX_Text(pDX, IDC_EDIT5, R);
	DDX_Control(pDX, IDC_SCROLLBAR3, SCRVert);
	DDX_Control(pDX, IDC_SCROLLBAR4, SCRHor);
}

BEGIN_MESSAGE_MAP(CSignalPropagationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CSignalPropagationDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CSignalPropagationDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSignalPropagationDlg::OnBnClickedButton2)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()


// Обработчики сообщений CSignalPropagationDlg

BOOL CSignalPropagationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	gdiObjLocation.d = d_size;
	gdiObjLocation.N = N_size;
	SCRVert.SetScrollRange(-45, 45);
	SCRVert.SetScrollPos(20);
	SCRHor.SetScrollRange(-45, 45);
	SCRHor.SetScrollPos(20);
	Vert = 20; Hor=20;
	DotsInChart(dots);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CSignalPropagationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CSignalPropagationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CSignalPropagationDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}


void CSignalPropagationDlg::OnBnClickedButton2() //Сброс
{
	UpdateData(1);
	gdiObjLocation.sources.clear();
	gdiObjLocation.d = d_size;
	gdiObjLocation.N = N_size;
	gdiObjLocation.Invalidate();
	gdiObjSignals.Invalidate();
	sources.clear();
	dots.clear();
	UpdateData(0);
}

void CSignalPropagationDlg::OnBnClickedButton1() //построить 2d и 3d модели
{
	GDISourceNormalizationGet();
	gdiObjLocation.sources.clear();
	dots.clear();
	for (int i = 0; i < sources.size(); i++)
	{
		CPoint buf;
		buf.x = sources[i].x / N_size / d_size * gdiObjLocation.Xpix; 
		buf.y = sources[i].y / N_size / d_size * gdiObjLocation.Ypix;
		gdiObjLocation.sources.push_back(buf);
	}
	gdiObjLocation.Invalidate();
	double step = 2*R / M_size;
	vector <vector<double>> gdiCell;
	dots.resize(M_size);
	gdiCell.resize(M_size);
	for (int i = 0; i < dots.size(); i++)
	{
		dots[i].resize(M_size);
		gdiCell[i].resize(M_size);
		for (int j = 0; j < dots[i].size(); j++)
		{
			dots[i][j].x = i * step -R;
			dots[i][j].y = j * step - R;
			double L2d = sqrt((dots[i][j].x) * (dots[i][j].x) + (dots[i][j].y) * (dots[i][j].y));			
			if (L2d > R)
			{
				dots[i][j].I = 0;
				dots[i][j].z = 0;
			}
			else
			{
				dots[i][j].z = sqrt(abs(pow(R, 2) - pow(dots[i][j].x, 2) - pow(dots[i][j].y, 2)));
				double re = 0;
				double im = 0;
				for (int k = 0; k < sources.size(); k++)
				{
					double r_0 = sqrt((dots[i][j].x - sources[k].x ) * (dots[i][j].x - sources[k].x) + \
						(dots[i][j].y - sources[k].y) * (dots[i][j].y - sources[k].y) + \
						(dots[i][j].z) * (dots[i][j].z));
					re += cos(2 * 3.1415926535 * r_0 / lymda) / r_0;
					im += sin(2 * 3.1415926535 * r_0 / lymda) / r_0;
				}
				dots[i][j].I = sqrt(re * re + im * im);
				gdiCell[i][j] = dots[i][j].I;
			}
		}
	}
	gdiObjSignals.GradientCell(gdiCell);
	gdiObjSignals.MyPaint();
	DotsInChart(dots);
}

void CSignalPropagationDlg::GDISourceNormalizationGet() //перенормировка координат
{
	sources.clear();
	for (int i = 0; i < gdiObjLocation.sources.size(); i++)
	{
		Source Buffer;
		Buffer.x = (double)gdiObjLocation.sources[i].x / gdiObjLocation.Xpix * N_size * d_size;
		Buffer.y = (double)gdiObjLocation.sources[i].y / gdiObjLocation.Ypix * N_size * d_size;
		Buffer.x = GetBliz(Buffer.x);
		Buffer.y = GetBliz(Buffer.y);
		sources.push_back(Buffer);
	}
}
double CSignalPropagationDlg::GetBliz(double x) //получение ближайшего целого числа
{
	x /= d_size;
	int k = ceil(x);
	double mod1, mod2;
	mod1 = abs(x - k); mod2 = abs(x - (k - 1));
	if (mod1 < mod2) return k * d_size;
	else return((k - 1) * d_size);
}
DoubleArray CSignalPropagationDlg::vectorToArray(std::vector<double>& v) //функция для chart direct
{
	return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
}
void CSignalPropagationDlg::DotsInChart(vector <vector<Dot>>& dots) //построение 3d модели
{
	viewer.clearAllRanges();
	std::vector<double> Ve_x;
	std::vector<double> Ve_y;
	std::vector<double> Ve_z;

	for (int i = 0; i < dots.size(); i++)
	{
		for (int j = 0; j < dots[i].size(); j++)
		{
			Ve_x.push_back(dots[i][j].x);
			Ve_y.push_back(dots[i][j].y);
			Ve_z.push_back(dots[i][j].I);
		}
	}
	SurfaceChart* c = new SurfaceChart(790, 480);

	c->setPlotRegion(360, 220, 310, 330, 230);

	// Set the elevation and rotation angles to 45 and -45 degrees
	c->setViewAngle(Vert, Hor);

	// Set the perspective level to 30
	//c->setPerspective(30);

	//------------------------------
	c->zAxis()->setAutoScale(0, 0, 1);
	//-------------------------------
	// Set the data to use to plot the chart
	c->setData(vectorToArray(Ve_x), vectorToArray(Ve_y), vectorToArray(Ve_z));

	ColorAxis* cAxis = c->setColorAxis(740, 40, Chart::TopRight, 270, Chart::Right);

	cAxis->setTitle("Amplitude", "arialbd.ttf", 12);

	// Set surface grid lines to semi-transparent black (cc000000)
	c->setSurfaceAxisGrid(0xcc000000);

	// Set contour lines to semi-transparent white (80ffffff)
	c->setContourColor(0x80ffffff);

	// Set the wall thickness to 0
	c->setWallThickness(10, 0, 0);

	// Show only the xy wall, and hide the yz and zx walls.
	c->setWallVisibility(true, true, true);

	// Set the x, y and z axis titles using 12 points Arial Bold font
	if (1)
	{
		c->xAxis()->setTitle("Ox", "arialbd.ttf", 12);
		c->yAxis()->setTitle("Oy", "arialbd.ttf", 12);
	}
	viewer.setChart(c);	//delete c;
	delete c;
}
void CSignalPropagationDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollBar* pSB = NULL;
	pSB = (CScrollBar*)this->GetDlgItem(IDC_SCROLLBAR3);

	switch (nSBCode)
	{
	case SB_THUMBPOSITION:
	{
		pSB->SetScrollPos(nPos);
		break;
	}
	}
	if (dots.size() != 0)
	{
		int ux, uy;
		ux = SCRHor.GetScrollPos();
		uy = SCRVert.GetScrollPos();
		if ((Vert - uy) != 0)
		{
			Vert = uy;
			DotsInChart(dots);
		}		
	}

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}


void CSignalPropagationDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	CScrollBar* pSB = NULL;
	pSB = (CScrollBar*)this->GetDlgItem(IDC_SCROLLBAR4);

	switch (nSBCode)
	{
	case SB_THUMBPOSITION:
	{
		pSB->SetScrollPos(nPos);
		break;
	}
	}
	if (dots.size() != 0)
	{
		int ux, uy;
		ux = SCRHor.GetScrollPos();
		uy = SCRVert.GetScrollPos();
		if ((Hor - ux) != 0)
		{
			Hor = ux;
			DotsInChart(dots);
		}
	}
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}
