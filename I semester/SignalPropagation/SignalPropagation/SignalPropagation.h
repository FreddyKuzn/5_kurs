﻿
// SignalPropagation.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CSignalPropagationApp:
// Сведения о реализации этого класса: SignalPropagation.cpp
//

class CSignalPropagationApp : public CWinApp
{
public:
	CSignalPropagationApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CSignalPropagationApp theApp;
