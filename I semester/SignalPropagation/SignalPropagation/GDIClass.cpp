#include "pch.h"
#include "GDIClass.h"
#include "SignalPropagation.h"
#include "SignalPropagationDlg.h"
using namespace Gdiplus;
IMPLEMENT_DYNAMIC(GDIClass, CStatic)


GDIClass::GDIClass()
{
	GdiplusStartupInput gdiPlusStartapInput;
	if (GdiplusStartup(&gdiPlusToken, &gdiPlusStartapInput, NULL) != Ok)
	{
		MessageBox(L"Init error", L"Error Message", MB_OK);
		exit(0);
	}
}
GDIClass::~GDIClass()
{
	GdiplusShutdown(gdiPlusToken);
}

BEGIN_MESSAGE_MAP(GDIClass, CStatic)
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()

 void GDIClass::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	 Graphics g(lpDrawItemStruct->hDC);
	 Bitmap Map(lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		 lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1,
		 &g);
	 Graphics g2(&Map);
	 g2.SetSmoothingMode(SmoothingModeHighSpeed);
	 Color A((BYTE)255, (BYTE)255, (BYTE)255);
	 g2.Clear(A);
	 Matrix matr;
	 g2.ResetTransform();

	 xmax = N* zoom;
	 ymax = N* zoom;
	 Xpix = lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left;
	 Ypix = lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top;
	 Gdiplus::REAL kx = (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) / (double)(xmax - xmin); //���������� ������������
	 Gdiplus::REAL ky = (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) / (double)(ymax - ymin);
	 matr.Scale(kx, ky);
	 g2.SetTransform(&matr);

	 SolidBrush kist(Color::White);
	 Pen BLACK(Color::Black, 1);
	 Pen RED(Color::Red, 1);
	 Pen BLUE(Color::Blue, (int)(xmax/100));

	 vector <CPoint> sourcesBuffer = sources;
	 for (int i = 0; i < sourcesBuffer.size(); i++)
	 {
		 sourcesBuffer[i].x = (double)sourcesBuffer[i].x / (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) * xmax;
		 sourcesBuffer[i].y = (double)sourcesBuffer[i].y / (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) * ymax;
	 }

	 for (int i = 0; i < N*zoom; i+=zoom)
	 {
		 g2.DrawLine(&BLACK, i, ymax, i, ymin);
		 g2.DrawLine(&BLACK, xmax, i, xmin, i);
	 }
	 g2.DrawLine(&RED, xmax/2, ymax, xmax/2, ymin);
	 g2.DrawLine(&RED, xmax, ymax/2, xmin, ymax/2);


	 for (int i = 0; i < sourcesBuffer.size(); i++)
	 {
		 g2.DrawEllipse(&BLUE, sourcesBuffer[i].x-0.25*zoom, sourcesBuffer[i].y-0.25*zoom,zoom/2, zoom/2);
	 }
	 g.DrawImage(&Map, 0, 0);
}

 void GDIClass::OnLButtonDblClk(UINT nFlags, CPoint point)
 {
	 CPoint source_buf;
	 source_buf.x = (double)point.x;
	 source_buf.y = (double)point.y;
	 sources.push_back(source_buf);
	 Invalidate();
	 CStatic::OnLButtonDblClk(nFlags, point);
 }
