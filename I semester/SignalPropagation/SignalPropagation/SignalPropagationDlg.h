﻿
// SignalPropagationDlg.h: файл заголовка
//

#include <vector>
#include <math.h>
#include "GDIClass.h"
#include "GDIClassGrad.h"
#include "ChartViewer.h"
using namespace std;
#define M_PI 3.1415926535
// Диалоговое окно CSignalPropagationDlg
class CSignalPropagationDlg : public CDialogEx
{
// Создание
public:
	CSignalPropagationDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SIGNALPROPAGATION_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	GDIClass gdiObjLocation;
	GDIClassGrad gdiObjSignals;
	CChartViewer viewer;
	DoubleArray vectorToArray(std::vector<double>& v);
	int Vert; int Hor;

	afx_msg void OnBnClickedButton2();
	void GDISourceNormalizationGet();
	int N_size;
	double d_size;
	struct Source
	{
		double x = 0;
		double y = 0;
	};
	struct Dot
	{
		double x = 0;
		double y = 0;
		double z = 0;
		double I = 0;
	};
	vector <Source> sources;
	vector <vector<Dot>> dots;
	double GetBliz(double x); //получение ближайшей целой координаты
	void DotsInChart(vector <vector<Dot>>&dots);
	int M_size;
	double lymda;
	double R;
	CScrollBar SCRVert;
	CScrollBar SCRHor;
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
