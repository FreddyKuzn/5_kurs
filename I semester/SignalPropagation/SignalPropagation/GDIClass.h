#pragma once
#include <afxwin.h>
#include <math.h>
#include <vector>
#include <gdiplus.h>

using namespace std;
#define M_PI 3.1415926535
class GDIClass :
	public CStatic
{
	//////////////////////////////////////////
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
	ULONG_PTR gdiPlusToken;
protected:
	DECLARE_MESSAGE_MAP()
public:
	int N;
	double d;
	vector <CPoint> sources;

	int xmax = 100;
	int xmin = 0;
	int ymax = 100;
	int ymin = 0;
	int zoom = 100;
	int Xpix, Ypix;
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);

};

