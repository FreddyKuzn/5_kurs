﻿
// IzingModelDlg.h: файл заголовка
//

#pragma once
#include <vector>
#include "GDIClass.h"
#include "ChartViewer.h"
#include <string>
#include "cubic.h"

using namespace std;

// Диалоговое окно CIzingModelDlg
class CIzingModelDlg : public CDialogEx
{
	// Создание
public:
	CIzingModelDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IZINGMODEL_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();

	int N_size;
	GDIClass gdi_obj;
	vector<vector<vector<int>>> table;
	double k_bol = 1.38064852 * pow(10, -23);
	double Ecm;
	double T_koeff;
	CProgressCtrl ProgressC;
	CChartViewer viewerEcm;
	CChartViewer viewerC;
	vector<double> study_Ecm;
	vector<double> inter_study_Ecm;
	vector<double> study_C;
	double T_min, T_max;
	int studys_dots;
	CString DebugStr;

	void Init(vector<vector<vector<int>>>& data);
	void gran(vector<vector<vector<int>>>& data); //изменение границ
	void MKIII(vector<vector<vector<int>>>& data, int N);
	double deltaH(vector<vector<vector<int>>>& data, int x1, int y1, int z1);
	double Emedium(vector<vector<vector<int>>>& data);
	void ViewerDraw(vector<double>& data, double Xmin, double Xmax, CChartViewer& viewer_num,string graf_type);
	DoubleArray vectorToArray(std::vector<double>& v)
	{
		return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
	}
	void gdiPaint(int sr,int orta)
	{
		if (table.size() <= 1) return;
		vector<vector<vector<int>>> table_buf;
		if (orta == 1)table_buf = table;
		if (orta == 2|| orta == 3)
		{
			table_buf.resize(table.size());
			for (int i = 0; i < table.size(); i++)
			{
				table_buf[i].resize(table[i].size());
				for (int j = 0; j < table[i].size(); j++)
				{
					table_buf[i][j].resize(table[i][j].size());
					for (int k = 0; k < table[i][j].size(); k++)
					{
						if (orta == 2)table_buf[i][j][k] = table[j][i][k];
						if (orta == 3)table_buf[i][j][k] = table[k][j][i];
					}
				}
			}
		}
		vector<vector<bool>> spin; spin.resize(table_buf.size());
		for (int i = 0; i < spin.size(); i++)
		{
			spin[i].resize(table_buf.size());
			for (int j = 0; j < spin[i].size(); j++)
			{
				if (table_buf[sr][i][j] == 1)
					spin[i][j] = true;
				else
					spin[i][j] = false;
			}
		}
		gdi_obj.DotsToRound(table_buf.size(), spin);
		gdi_obj.MyPaint(table_buf.size());
	}
	afx_msg void OnBnClickedButton5();
	int Pic_out_number;
	int MKIII_size;
	int Orta;
	afx_msg void OnBnClickedButton6();
};
