#pragma once
#include <afxwin.h>
#include <math.h>
#include <vector>

using namespace std;
#define M_PI 3.1415926535
class GDIClass :
	public CStatic
{
	//////////////////////////////////////////
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
protected:
	DECLARE_MESSAGE_MAP()
public:
	int xmax = 100;
	int xmin = 0;
	int ymax = 100;
	int ymin = 0;
	INT32 zoom = 1000;
	struct GDIDot //�����
	{
		INT32 x0;
		INT32 y0;
		bool type=0;
	};
	std::vector <GDIDot> massDots;
	void MyPaint(int N)
	{
		N += 2;
		xmax = N * zoom;
		ymax = N * zoom;
		Invalidate();
	}
	void DotsToRound(int size, vector<vector<bool>> &spin)
	{
		massDots.clear();
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size;j++)
			{
				GDIDot buf;
				buf.x0 = (i * zoom);
				buf.y0 = (j * zoom);
				buf.type = spin[i][j];
				massDots.push_back(buf);
			}			
		}
	}
};

