﻿
// IzingModelDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "IzingModel.h"
#include "IzingModelDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CIzingModelDlg



CIzingModelDlg::CIzingModelDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IZINGMODEL_DIALOG, pParent)
	, N_size(20)
	, Ecm(-1)
	, T_koeff(1)
	, DebugStr(_T(""))
	, Pic_out_number(10)
	, MKIII_size(200)
	, Orta(1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CIzingModelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, N_size);
	DDX_Control(pDX, IDC_GDI, gdi_obj);
	DDX_Text(pDX, IDC_EDIT2, Ecm);
	DDX_Text(pDX, IDC_EDIT3, T_koeff);
	DDX_Control(pDX, IDC_PROGRESS1, ProgressC);
	DDX_Control(pDX, IDC_GRAFIK_EMED, viewerEcm);
	DDX_Control(pDX, IDC_GRAFIK_C, viewerC);
	DDX_Text(pDX, IDC_EDIT5, DebugStr);
	DDX_Text(pDX, IDC_EDIT4, Pic_out_number);
	DDX_Text(pDX, IDC_EDIT6, MKIII_size);
	DDX_Text(pDX, IDC_EDIT7, Orta);
	DDV_MinMaxInt(pDX, Orta, 1, 3);
}

BEGIN_MESSAGE_MAP(CIzingModelDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CIzingModelDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CIzingModelDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CIzingModelDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CIzingModelDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CIzingModelDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CIzingModelDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CIzingModelDlg::OnBnClickedButton6)
END_MESSAGE_MAP()


// Обработчики сообщений CIzingModelDlg

BOOL CIzingModelDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	T_min = 1.5;
	T_max=2.5;
	studys_dots = 21;

	gdi_obj.MyPaint(N_size);
	ProgressC.SetRange(0, 100);
	ProgressC.SetPos(0);
	ViewerDraw(study_Ecm, T_min, T_max, viewerEcm,"e(T)");
	ViewerDraw(study_C, T_min, T_max, viewerC, "C(T)");
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CIzingModelDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CIzingModelDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CIzingModelDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CIzingModelDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}

void CIzingModelDlg::OnBnClickedButton5()
{
	UpdateData(1);
	gdiPaint(Pic_out_number, Orta);
	UpdateData(0);
}

void CIzingModelDlg::OnBnClickedButton1()
{
	UpdateData(1);
	Init(table);
	gran(table);
	gdiPaint(Pic_out_number, Orta);
	UpdateData(0);
}
void CIzingModelDlg::OnBnClickedButton2()
{
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	if (table.size() == 0) return;
	ProgressC.SetRange(0, MKIII_size);
	ProgressC.SetPos(0);
	for (int i = 0; i < MKIII_size; i++)
	{
		MKIII(table, N_size);
		ProgressC.SetPos(i+1);
	}
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	gdiPaint(Pic_out_number, Orta);
	UpdateData(0);
}

void CIzingModelDlg::OnBnClickedButton3() //Исследование
{	
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	int first_progon = 1000;
	int second_progon = 2000;
	DebugStr = "Первоначальное усреднение...";
	study_Ecm.clear();
	study_Ecm.resize(studys_dots);
	Init(table);
	gran(table);
	T_koeff = T_min;
	double T_step = (T_max - T_min) / studys_dots;
	ProgressC.SetRange(0, first_progon-1);
	ProgressC.SetPos(0);
	for (int i = 0; i < first_progon; i++)
	{
		MKIII(table, N_size);
		ProgressC.SetPos(i);
	}
	ProgressC.SetRange(0, second_progon-1);
	ProgressC.SetPos(0);
	for (int i = 0; i < studys_dots; i++)
	{
		T_koeff = T_min + i*T_step;
		for (int j = 0; j < second_progon; j++)
		{
			MKIII(table, N_size);
			ProgressC.SetPos(j);
		}
		study_Ecm[i] += Emedium(table);
	}
	for (int i = studys_dots-1; i >= 0; i--)
	{
		T_koeff = T_min + i * T_step;
		for (int j = 0; j < second_progon; j++)
		{
			MKIII(table, N_size);
			ProgressC.SetPos(j);
		}
		study_Ecm[i] += Emedium(table);
		study_Ecm[i] /= 2.;
	}
	ViewerDraw(study_Ecm, T_min, T_max, viewerEcm, "e(T)");
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	UpdateData(0);
}
void CIzingModelDlg::OnBnClickedButton4() //Интерполяция и дифференцирование
{
	UpdateData(1);
	inter_study_Ecm.clear();
	vector<double>X;
	vector<double>Y;
	for (int i = 0; i < study_Ecm.size(); i++)
	{
		X.push_back(i);
		Y.push_back(study_Ecm[i]);
	}
	tk::spline s;
	s.set_points(X, Y);
	for (size_t i = 0; i < study_Ecm.size()*10; i++)
	{
		double x = 0.1 * i;
		inter_study_Ecm.push_back(s(x));
	}
	/////////////////////////////////////////////////////////////////
	double T_step = (T_max - T_min) / inter_study_Ecm.size();
	study_C.clear();
	study_C.resize(inter_study_Ecm.size() - 1);
	double max_id = 0; double max_el = 0;
	for (size_t i = 0; i < study_C.size(); i++)
	{
		study_C[i] = inter_study_Ecm[i + 1] - inter_study_Ecm[i];
		if (study_C[i] > max_el)
		{
			max_el = study_C[i];
			max_id = T_min + (i) * T_step;
		}
	}
	string buffstr = "Tc = " + std::to_string(max_id) + ".";
	DebugStr = buffstr.c_str();
	ViewerDraw(inter_study_Ecm, T_min, T_max, viewerEcm,"e(T)");
	ViewerDraw(study_C, T_min, T_max, viewerC, "C(T)");
	UpdateData(0);
}
void CIzingModelDlg::Init(vector<vector<vector<int>>>& data)
{
	data.clear();
	data.resize(N_size + 2);
	for (int i = 0; i < data.size(); i++)
	{
		data[i].resize(N_size + 2);
		for (int j = 0; j < data[i].size(); j++)
		{
			data[i][j].resize(N_size + 2);
			for (int k = 0; k < data[i][j].size(); k++)
			{
				data[i][j][k] = 0;
			}
		}
	}
	int iter = 0;
	//s rand(time(0));
	do
	{
		int x = 1 + rand() % (N_size);
		int y = 1 + rand() % (N_size);
		int z = 1 + rand() % (N_size);

		if (data[x][y][z] != 1)
		{
			data[x][y][z] = 1;
			iter++;
		}

	} while (iter < (N_size * N_size * N_size) / 2);
}
void CIzingModelDlg::gran(vector<vector<vector<int>>>& data)
{
	for (int i = 0; i < data.size(); i++)
	{
		for (int j = 0; j < data.size(); j++)
		{
			data[0][i][j] = data[data.size() - 2][i][j];
			data[data.size() - 1][i][j] = data[1][i][j];

			data[i][0][j] = data[i][data.size() - 2][j];
			data[i][data.size() - 1][j] = data[i][1][j];

			data[i][j][0] = data[i][j][data.size() - 2];
			data[i][j][data.size() - 1] = data[i][j][1];
		}
	}
}
void CIzingModelDlg::MKIII(vector<vector<vector<int>>>& data, int N)
{
	double Temp = abs(Ecm)*T_koeff /k_bol;
	int NNN = N * N * N;
	int iter = 0;
	int iter_neg = 0;
	do
	{
		int x1, x2, y1, y2, z1, z2;
		for (;;)
		{
			x1 = 1 + rand() % (N_size);
			y1 = 1 + rand() % (N_size);
			z1 = 1 + rand() % (N_size);
			x2 = 1 + rand() % (N_size);
			y2 = 1 + rand() % (N_size);
			z2 = 1 + rand() % (N_size);
			if (table[x1][y1][z1] != table[x2][y2][z2])
				break;
		}
	
		
		double firstH = -(Ecm * 2) * (deltaH(table, x1, y1, z1) + deltaH(table, x2, y2, z2));
		int buf = table[x1][y1][z1];
		table[x1][y1][z1] = table[x2][y2][z2];
		table[x2][y2][z2] = buf;
		gran(table);
		double secondH = -(Ecm * 2) * (deltaH(table, x1, y1, z1) + deltaH(table, x2, y2, z2));
		double delta = secondH -firstH;
		
		if (delta > 0)
		{
			iter_neg++;
			double R = 0 + 1.*rand()/RAND_MAX;
			double myExp = exp(-delta / (k_bol * Temp));
			if (R > myExp)
			{
				buf = table[x1][y1][z1];
				table[x1][y1][z1] = table[x2][y2][z2];
				table[x2][y2][z2] = buf;
				gran(table);

			}
		}
	  
		iter++;
	} while (iter < NNN);
	iter = iter_neg;
}
double CIzingModelDlg::deltaH(vector<vector<vector<int>>>& data, int x1, int y1, int z1)
{
	double res = data[x1][y1][z1] * (data[x1+1][y1][z1] + data[x1-1][y1][z1] + data[x1][y1+1][z1] + data[x1][y1-1][z1] + data[x1][y1][z1+1] + data[x1][y1][z1-1]);
	return res;
}
double CIzingModelDlg::Emedium(vector<vector<vector<int>>>& data)
{
	double Buf = 0;
	if (data.empty()) return 0;
	for (int i = 1; i < data.size()-1; i++)
	{
		for (int j = 1; j < data[i].size()-1; j++)
		{
			for (int k = 1; k < data[i][j].size()-1; k++)
			{
				Buf+= data[i][j][k] * (data[i+1][j][k] + data[i-1][j][k] + data[i][j+1][k] + data[i][j-1][k] + data[i][j][k+1] + data[i][j][k-1]);
			}
		}
	}
	if (Ecm<0) return  (((-Ecm / 2) * Buf) / (pow(data.size() - 2, 3))) + Ecm;
	else 	return ((-Ecm / 2) * Buf)/(pow(data.size()-2,3));
}
void CIzingModelDlg::ViewerDraw(vector<double>& data, double Xmin, double Xmax, CChartViewer& viewer_num, string graf_type)
{
	
	// In this example, we simply use random data for the 3 data series.
	DoubleArray Arr_dataReal = vectorToArray(data);
	vector<double>Datatime;
	double OXstep;
	if (!data.empty())OXstep = (Xmax - Xmin) / (data.size()-1);
	else OXstep = (Xmax - Xmin) / 10.;
	for (double i = Xmin; i <= Xmax; i += OXstep)Datatime.push_back(i);
	DoubleArray timeStamps = vectorToArray(Datatime);

	// Create a XYChart object of size 600 x 400 pixels
	XYChart* c = new XYChart(600, 240);

	// Add a title box using grey (0x555555) 20pt Arial font
	//c->addTitle("", "arial.ttf", 20, 0x555555);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	c->setPlotArea(50, 50, 520, 120, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox* b = c->addLegend(20, 5, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	// Set axis label font to 12pt Arial
	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	c->xAxis()->setColors(Chart::TextColor, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::TextColor);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	c->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	c->xAxis()->setTickDensity(80);
	c->yAxis()->setTickDensity(40);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	c->yAxis()->setTitle("", "arial.ttf", 14, 0x555555);

	// Add a line layer to the chart with 3-pixel line width
	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);

	// Add 3 data series to the line layer
	const char* chPathPic = graf_type.c_str();
	layer->addDataSet(Arr_dataReal, 0x5588cc, chPathPic);
	// The x-coordinates for the line layer
	layer->setXData(timeStamps);
	viewer_num.setChart(c);
	//const char* chPathPic = PathPic.c_str();
	//c->makeChart(chPathPic);
	delete c;
}

void CIzingModelDlg::OnBnClickedButton6()
{
	DoubleArray data = vectorToArray(study_Ecm);

	// Create a XYChart object of size 600 x 300 pixels
	XYChart* c = new XYChart(600, 240);

	// Set the plotarea at (50, 35) and of size 500 x 240 pixels. Enable both the horizontal and
	// vertical grids by setting their colors to grey (0xc0c0c0)
	c->setPlotArea(50, 50, 520, 150)->setGridColor(0xc0c0c0, 0xc0c0c0);

	// Add a title to the chart using 18 point Times Bold Itatic font.
	//c->addTitle("LOWESS Generic Curve Fitting Algorithm", "timesbi.ttf", 18);

	// Set the y axis line width to 3 pixels
	c->yAxis()->setWidth(3);

	// Add a title to the x axis using 12pt Arial Bold Italic font
	//c->xAxis()->setTitle("Server Load (TPS)", "arialbi.ttf", 12);

	// Set the x axis line width to 3 pixels
	c->xAxis()->setWidth(3);

	// Set the x axis scale from 0 - 50, with major tick every 5 units and minor tick every 1 unit
	c->xAxis()->setLinearScale(0, study_Ecm.size()-1, 5, 1);

	// Add a blue layer to the chart
	LineLayer* layer = c->addLineLayer();

	// Add a red (0x80ff0000) data set to the chart with square symbols
	layer->addDataSet(data, 0x80ff0000)->setDataSymbol(Chart::SquareSymbol);

	// Set the line width to 2 pixels
	layer->setLineWidth(2);

	// Use lowess for curve fitting, and plot the fitted data using a spline layer with line width
	// set to 3 pixels
	c->addSplineLayer(ArrayMath(data).lowess(), 0x0000ff)->setLineWidth(3);

	// Set zero affinity to 0 to make sure the line is displayed in the most detail scale
	c->yAxis()->setAutoScale(0, 0, 0);
	c->makeChart("curvefitting.png");
	// Output the chart
	viewerEcm.setChart(c);
	//free up resources
	delete c;
}
