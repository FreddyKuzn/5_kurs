#include "pch.h"
#include "GDIClass.h"
#include "IzingModel.h"
#include "IzingModelDlg.h"
#include <ctime>
using namespace Gdiplus;
IMPLEMENT_DYNAMIC(GDIClass, CStatic)
ULONG_PTR gdiPlusToken;
GDIClass::GDIClass()
{
	GdiplusStartupInput gdiPlusStartapInput;
	if (GdiplusStartup(&gdiPlusToken, &gdiPlusStartapInput, NULL) != Ok)
	{
		MessageBox(L"Init error", L"Error Message", MB_OK);
		exit(0);
	}
}
GDIClass::~GDIClass()
{
	GdiplusShutdown(gdiPlusToken);
}

BEGIN_MESSAGE_MAP(GDIClass, CStatic)
END_MESSAGE_MAP()

void GDIClass::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	Graphics g(lpDrawItemStruct->hDC);
	Bitmap Map(lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1,
		&g);
	Graphics g2(&Map);
	g2.SetSmoothingMode(SmoothingModeHighSpeed);
	Color A((BYTE)0, (BYTE)0, (BYTE)0);
	g2.Clear(A);
	Matrix matr;
	g2.ResetTransform();
	Pen BLACK(Color::Black, 1);
	Pen RED(Color::Red, 1);
	Pen BLUE(Color::Blue, 1);
	SolidBrush kistRed(Color::Red);
	SolidBrush kistBlue(Color::Blue);
	Gdiplus::REAL kx = (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) / (double)(xmax - xmin); //���������� ������������
	Gdiplus::REAL ky = (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) / (double)(ymax - ymin);
	matr.Scale(kx, ky);
	g2.SetTransform(&matr);
	for (int i = 0; i < massDots.size(); i++)
	{
		//g2.DrawEllipse(&BLACK, massDots[i].x0- zoom/4, massDots[i].y0- zoom/4, zoom, zoom);
		if (massDots[i].type)
			g2.FillRectangle(&kistRed, massDots[i].x0 + zoom, massDots[i].y0 +zoom, zoom, zoom);
		else
			g2.FillRectangle(&kistBlue, massDots[i].x0 + zoom, massDots[i].y0 +zoom, zoom, zoom);
	}
	CLSID pngClsid;
	CLSIDFromString(L"{557CF406-1A04-11D3-9A73-0000F81EF32E}", &pngClsid);
	Map.Save(L"file.png", &pngClsid, NULL);
	g.DrawImage(&Map, 0, 0);
}