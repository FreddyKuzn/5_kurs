/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#include <fstream>
#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/XUnitPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    std::ofstream ofstr("TEST-cxxtest.xml");
    CxxTest::XUnitPrinter tmp(ofstr);
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::XUnitPrinter >( tmp, argc, argv );
    return status;
}
bool suite_myTestSuite_init = false;
#include "myTestSuite.h"

static myTestSuite suite_myTestSuite;

static CxxTest::List Tests_myTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_myTestSuite( "C:/DriveD/5kurs/workspace/DateTime/ConsoleTime/myTestSuite.h", 6, "myTestSuite", suite_myTestSuite, Tests_myTestSuite );

static class TestDescription_suite_myTestSuite_test_cross_month : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_cross_month() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 47, "test_cross_month" ) {}
 void runTest() { suite_myTestSuite.test_cross_month(); }
} testDescription_suite_myTestSuite_test_cross_month;

static class TestDescription_suite_myTestSuite_test_cross_february : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_cross_february() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 58, "test_cross_february" ) {}
 void runTest() { suite_myTestSuite.test_cross_february(); }
} testDescription_suite_myTestSuite_test_cross_february;

static class TestDescription_suite_myTestSuite_test_error_february : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_error_february() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 69, "test_error_february" ) {}
 void runTest() { suite_myTestSuite.test_error_february(); }
} testDescription_suite_myTestSuite_test_error_february;

static class TestDescription_suite_myTestSuite_test_error_data_1 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_error_data_1() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 81, "test_error_data_1" ) {}
 void runTest() { suite_myTestSuite.test_error_data_1(); }
} testDescription_suite_myTestSuite_test_error_data_1;

static class TestDescription_suite_myTestSuite_test_error_data_2 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_error_data_2() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 93, "test_error_data_2" ) {}
 void runTest() { suite_myTestSuite.test_error_data_2(); }
} testDescription_suite_myTestSuite_test_error_data_2;

static class TestDescription_suite_myTestSuite_test_error_data_3 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_error_data_3() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 105, "test_error_data_3" ) {}
 void runTest() { suite_myTestSuite.test_error_data_3(); }
} testDescription_suite_myTestSuite_test_error_data_3;

static class TestDescription_suite_myTestSuite_test_error_data_4 : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_myTestSuite_test_error_data_4() : CxxTest::RealTestDescription( Tests_myTestSuite, suiteDescription_myTestSuite, 116, "test_error_data_4" ) {}
 void runTest() { suite_myTestSuite.test_error_data_4(); }
} testDescription_suite_myTestSuite_test_error_data_4;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
