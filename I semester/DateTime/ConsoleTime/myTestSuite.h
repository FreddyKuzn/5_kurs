﻿#pragma once
#include <cxxtest/TestSuite.h>
#include <iostream>
#include "time.h"
#include "GetData_lib.h"
class myTestSuite : public CxxTest::TestSuite
{
public:
	//void testIn0()
	//{
	//	int test_test = 3 + 2;
	//	TS_ASSERT(test_test == 5);
	//}
	//void testIn1()
	//{
	//	Time hello;
	//	std::string test_st = "00:00";
	//	Time::Errors test_er = hello.TimeIn(test_st);
	//	TS_ASSERT(test_er == Time::Done);
	//}
	//void testIn2_1()
	//{
	//	Time hello;
	//	std::string test_st = "00";
	//	Time::Errors test_er = hello.TimeIn(test_st);
	//	TS_ASSERT(test_er == Time::Error_In);
	//}
	//void testIn2_2()
	//{
	//	Time hello;
	//	std::string test_st = "Hello";
	//	Time::Errors test_er = hello.TimeIn(test_st);
	//	TS_ASSERT(test_er == Time::Error_In);
	//}
	//void testIn3()
	//{
	//	Time hello;
	//	std::string test_st = "12:00";
	//	Time::Errors test_er = hello.TimeIn(test_st);
	//	hello.ChTime(1, 0);
	//	std::string test_out;
	//	hello.TimeOut(test_out);
	//	TS_ASSERT(test_er == Time::Done);
	//	TS_ASSERT(test_out == "Тринадцать часов ноль минут");
	//}
	////////////////////////////////////////////////////////////////////////
	void test_cross_month()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "12.31";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "третье января\n");
		TS_ASSERT(err == Date::Success);
	}
	void test_cross_february()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "02.28";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "третье марта\n");
		TS_ASSERT(err == Date::Success);
	}
	void test_error_february()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "02.29";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "");		
		TS_ASSERT(err == Date::Error_In);
	}

	void test_error_data_1()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "01";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "");
		TS_ASSERT(err == Date::Error_In);

	}
	void test_error_data_2()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "Hello";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "");
		TS_ASSERT(err == Date::Error_In);

	}
	void test_error_data_3()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "21.32";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "");
		TS_ASSERT(err == Date::Error_In);
	}
	void test_error_data_4()
	{
		setlocale(LC_ALL, "rus");
		Date::Errors err;
		string In = "00.00";
		string Out;
		Date date;
		err = date.GetDataPlus3DaysStr(In, Out);
		TS_ASSERT(Out == "");
		TS_ASSERT(err == Date::Error_In);
	}
};