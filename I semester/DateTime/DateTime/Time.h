#pragma once
#ifdef DLL_EXPORT

#define DLLAPI _declspec(dllexport)
#else
#define DLLAPI _declspec(dllimport)
#endif

#include <vector>
#include <string>
#include <math.h>
#include <iostream>
using namespace std;
DLLAPI  class Time
{
protected:
	struct HH_MMst
	{
		vector <string> one;
		vector <string> two;
	};
	HH_MMst time_st;
public:
	enum Errors
	{
		Done, Error_In, Error_Out
	};
	struct HH_MM
	{
		int HH;
		int MM;
	};
	HH_MM time;
protected:
	void init();
	HH_MM Normal_Time(int& data);
public:
	/**
	@param HH: ��������� �����;
	@param HH: ��������� �����;
	@return void;
	*/
	void ChTime(int HH, int MM);
	/**
	������� ����� ������� ����� �������
	@return Errors;
	*/
	Errors TimeIn();
	/**
	������� ����� �������
	@param st: ������ �������;
	@return Errors;
	*/
	Errors TimeIn(string& st);
	/**
	������� ������ �������
	@param str: ������ �������;
	@return Errors;
	*/
	Errors TimeOut(string& str);
};
#define DLL_EXPORT