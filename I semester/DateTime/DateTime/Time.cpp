#define DLL_EXPORT
#include "Time.h"
void Time::init()
{
	time.HH = 0;
	time.MM = 0;
	time_st.one.clear();
	time_st.two.clear();
	time_st.one.resize(24);
	time_st.one[0] = "���� �����";
	time_st.one[1] = "���� ���";
	time_st.one[2] = "��� ����";
	time_st.one[3] = "��� ����";
	time_st.one[4] = "������ ����";
	time_st.one[5] = "���� �����";
	time_st.one[6] = "����� �����";
	time_st.one[7] = "���� �����";
	time_st.one[8] = "������ �����";
	time_st.one[9] = "������ �����";
	time_st.one[10] = "������ �����";
	time_st.one[11] = "����������� �����";
	time_st.one[12] = "���������� �����";
	time_st.one[13] = "���������� �����";
	time_st.one[14] = "������������ �����";
	time_st.one[15] = "���������� �����";
	time_st.one[16] = "����������� �����";
	time_st.one[17] = "���������� �����";
	time_st.one[18] = "������������ �����";
	time_st.one[19] = "������������ �����";
	time_st.one[20] = "�������� �����";
	time_st.one[21] = "�������� ���� ���";
	time_st.one[22] = "�������� ��� ����";
	time_st.one[23] = "�������� ��� ����";
	time_st.two.resize(60);
	time_st.two[0] = "���� �����";
	time_st.two[1] = "���� ������";
	time_st.two[2] = "��� ������";
	time_st.two[3] = "��� ������";
	time_st.two[4] = "������ ������";
	time_st.two[5] = "���� �����";
	time_st.two[6] = "����� �����";
	time_st.two[7] = "���� �����";
	time_st.two[8] = "������ �����";
	time_st.two[9] = "������ �����";
	time_st.two[10] = "������ �����";
	time_st.two[11] = "����������� �����";
	time_st.two[12] = "���������� �����";
	time_st.two[13] = "���������� �����";
	time_st.two[14] = "������������ �����";
	time_st.two[15] = "���������� �����";
	time_st.two[16] = "����������� �����";
	time_st.two[17] = "���������� �����";
	time_st.two[18] = "������������ �����";
	time_st.two[19] = "������������ �����";
	time_st.two[20] = "�������� �����";
	time_st.two[21] = "�������� ���� ������";
	time_st.two[22] = "�������� ��� ������";
	time_st.two[23] = "�������� ��� ������";
	time_st.two[24] = "�������� ������ ������";
	time_st.two[25] = "�������� ���� �����";
	time_st.two[26] = "�������� ����� �����";
	time_st.two[27] = "�������� ���� �����";
	time_st.two[28] = "�������� ������ �����";
	time_st.two[29] = "�������� ������ �����";
	time_st.two[30] = "�������� �����";
	time_st.two[31] = "�������� ���� ������";
	time_st.two[32] = "�������� ��� ������";
	time_st.two[33] = "�������� ��� ������";
	time_st.two[34] = "�������� ������ ������";
	time_st.two[35] = "�������� ���� �����";
	time_st.two[36] = "�������� ����� �����";
	time_st.two[37] = "�������� ���� �����";
	time_st.two[38] = "�������� ������ �����";
	time_st.two[39] = "�������� ������ �����";
	time_st.two[40] = "����� �����";
	time_st.two[41] = "����� ���� ������";
	time_st.two[42] = "����� ��� ������";
	time_st.two[43] = "����� ��� ������";
	time_st.two[44] = "����� ������ ������";
	time_st.two[45] = "����� ���� �����";
	time_st.two[46] = "����� ����� �����";
	time_st.two[47] = "����� ���� �����";
	time_st.two[48] = "����� ������ �����";
	time_st.two[49] = "����� ������ �����";
	time_st.two[50] = "��������� �����";
	time_st.two[51] = "��������� ���� ������";
	time_st.two[52] = "��������� ��� ������";
	time_st.two[53] = "��������� ��� ������";
	time_st.two[54] = "��������� ������ ������";
	time_st.two[55] = "��������� ���� �����";
	time_st.two[56] = "��������� ����� �����";
	time_st.two[57] = "��������� ���� �����";
	time_st.two[58] = "��������� ������ �����";
	time_st.two[59] = "��������� ������ �����";
}
Time::Errors Time::TimeIn()
{
	setlocale(LC_CTYPE, "rus");
	init();
	string bufst;
	std::cin >> bufst;
	if (bufst.size() > 5 || bufst.size() < 3)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	int pos = 0;
	int next = bufst.find(':', pos);
	if (next == string::npos)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	string exprHH(bufst.substr(pos, (next - pos)));
	try {
		time.HH = stoi(exprHH);
	}
	catch (...)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	if (time.HH > 23 || time.HH < 0)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	pos = next + 1;
	string exprMM = bufst.substr(pos, bufst.size());
	try
	{
		time.MM = stoi(exprMM);
	}
	catch (...)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	if (time.MM > 59 || time.MM < 0)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	return Done;
}
Time::Errors Time::TimeIn(string& st)
{
	setlocale(LC_CTYPE, "rus");
	init();
	string bufst = st;
	if (bufst.size() > 5 || bufst.size() < 3)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	int pos = 0;
	int next = bufst.find(':', pos);
	if (next == string::npos)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	string exprHH(bufst.substr(pos, (next - pos)));
	try
	{
		time.HH = stoi(exprHH);
	}
	catch (...)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	if (time.HH > 23 || time.HH < 0)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	pos = next + 1;
	string exprMM = bufst.substr(pos, bufst.size());
	try
	{
		time.MM = stoi(exprMM);
	}
	catch (...)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	if (time.MM > 59 || time.MM < 0)
	{
		//cout << "Error Date_In!\n" << endl;
		return Error_In;
	}
	return Done;
}
Time::Errors Time::TimeOut(string& str)
{
	setlocale(LC_CTYPE, "rus");
	str = "";
	str += time_st.one[time.HH];
	str += " ";
	str += time_st.two[time.MM];
	return Done;
}
void Time::ChTime(int HH, int MM)
{
	int all_time = time.HH * 60 + time.MM;
	int all_min = HH * 60 + MM;
	all_time += all_min;
	time = Normal_Time(all_time);
}
Time::HH_MM Time::Normal_Time(int& data)
{
	int st = 24 * 60;
	HH_MM rep;
	for (;;)
	{
		if (data > 0 && data < st)
		{
			double buf = (double)data / 60;
			rep.HH = (int)floor(buf);
			buf = data % 60;
			rep.MM = (int)buf;
			break;
		}
		if (data == 0 || data == st)
		{
			rep.HH = 0; rep.MM = 0;
			break;
		}
		if (data < 0)
		{
			data += st;
			continue;
		}
		if (data > st)
		{
			data -= st;
			continue;
		}
	}
	return rep;
}