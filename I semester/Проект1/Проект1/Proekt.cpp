#define DLL_EXPORT

#include <cmath>
#include "Proekt.h"

DLLAPI Errors Solve(double a, double b, double c, double& x1, double& x2, double& D)
{
	if (fabs(a) < 10.e-6)
	{
		return ErrorCode1;
	}

	D = b * b - 4 * a * c;

	if (fabs(D) < 10.e-6)
	{
		x2 = x1 = (-b) / (2 * a);
		return Succesful;
	}
	if (D > 10.e-6)
	{
		x1 = (-b + sqrt(D)) / (2 * a);
		x2 = (-b - sqrt(D)) / (2 * a);
		return Succesful;
	}

	if (D < 10.e-6)
	{
		return No_solution;
	}

}