#pragma once

#ifdef DLL_EXPORT

#define DLLAPI _declspec(dllexport)
#else
#define DLLAPI _declspec(dllimport)
#endif

enum Errors
{
	ErrorCode1, No_solution, Succesful
};


DLLAPI Errors Solve(double a, double d, double c, double& x1, double& x2, double& D);

#define DLL_EXPORT