#include <Proekt.h>
#include "SolveTest.h"
#include <iostream>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/XmlOutputterHook.h>
#include <fstream>
using namespace std;

int main()
{
	CppUnit::TextUi::TestRunner runner;
	ofstream fs("Test.xml");
	CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(), fs);
	outputter->setStyleSheet("report.xsl");
	runner.setOutputter(outputter);
	runner.addTest(SolveTest::suite());
	runner.run();
	fs.close();
	system("pause");
	return 0;
}