#include "SolveTest.h"

void SolveTest::Test1()
{
	Errors code;
	double x1, x2, D;
	code = Solve(1., 5., 4., x1, x2, D);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-1, x1, 1.e-6);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-4, x2, 1.e-6);
	CPPUNIT_ASSERT(code == Succesful);
}
void SolveTest::Test2()
{
	Errors code;
	double x1, x2, D;
	code = Solve(1., 6., 9., x1, x2, D);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-3, x1, 1.e-6);
	CPPUNIT_ASSERT_DOUBLES_EQUAL(-3, x2, 1.e-6);
	CPPUNIT_ASSERT(code == Succesful);
}
void SolveTest::Test3()
{
	Errors code;
	double x1, x2, D;
	code = Solve(1., 2., 26., x1, x2, D);
	CPPUNIT_ASSERT(code == No_solution);
}
void SolveTest::Test4()
{
	double x1, x2, D;
	int error = Solve(0., 2., 26., x1, x2, D);
	CPPUNIT_ASSERT(error == ErrorCode1);
}