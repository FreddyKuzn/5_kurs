#pragma once
#include <Proekt.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
//#define CPPUNITEX_NAMED_TEST (testMethod,name) CPPUNIT_TEST_SUITE_ADD_TEST((new CPPUNIT_NS::TestCaller <TestFixtureType> (name, &TestFixtureType::testMethod)));

class SolveTest : public CppUnit::TestFixture
{
public:
	void SetUp()
	{

	}
	void TearDown()
	{

	}
	void Test1();
	void Test2();
	void Test3();
	void Test4();
private:
	CPPUNIT_TEST_SUITE(SolveTest);
	CPPUNIT_TEST_SUITE_ADD_TEST((new CPPUNIT_NS::TestCaller <TestFixtureType>("2 solutions:Done!", &TestFixtureType::Test1)));
	CPPUNIT_TEST_SUITE_ADD_TEST((new CPPUNIT_NS::TestCaller <TestFixtureType>("1  solution:Done!", &TestFixtureType::Test2)));
	CPPUNIT_TEST_SUITE_ADD_TEST((new CPPUNIT_NS::TestCaller <TestFixtureType>("No solution:Done!", &TestFixtureType::Test3)));
	CPPUNIT_TEST_SUITE_ADD_TEST((new CPPUNIT_NS::TestCaller <TestFixtureType>("a==0:Done!", &TestFixtureType::Test4)));
	CPPUNIT_TEST_SUITE_END();
};