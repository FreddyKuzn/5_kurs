#include "pch.h"
#include "../HEXDEC/Header.h"
TEST(TestCaseName, TestName) {
	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}

TEST(HEXDEC_CaseDone, DoneIn1) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("0xA", a);
	EXPECT_EQ(a, 10);
	EXPECT_EQ(er, Convert::Done);
}
TEST(HEXDEC_CaseDone, DoneIn2) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("0xFF", a);
	EXPECT_EQ(a, 255);
	EXPECT_EQ(er, Convert::Done);
}
TEST(HEXDEC_CaseDone, DoneIn3) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("0x10", a);
	EXPECT_EQ(a, 16);
	EXPECT_EQ(er, Convert::Done);
}
TEST(HEXDEC_CaseDone, DoneIn4) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("+0x10", a);
	EXPECT_EQ(a, 16);
	EXPECT_EQ(er, Convert::Done);
}
TEST(HEXDEC_CaseDone, DoneIn5) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("-0x10", a);
	EXPECT_EQ(a, 16);
	EXPECT_EQ(er, Convert::Done);
}
TEST(HEXDEC_CaseError, ErrorIn1) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("0xXVHT", a);
	EXPECT_EQ(er, Convert::Error_In);
}
TEST(HEXDEC_CaseError, ErrorIn2) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("", a);
	EXPECT_EQ(er, Convert::Error_In);
}
TEST(HEXDEC_CaseError, ErrorIn3) {
	Convert A;
	int a;
	Convert::Errors er;
	er = A.In_Out_int("HelloWorld!", a);
	EXPECT_EQ(er, Convert::Error_In);
}
