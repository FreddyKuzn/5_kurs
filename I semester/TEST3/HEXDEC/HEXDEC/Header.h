#pragma once
#ifdef DLL_EXPORT

#define DLLAPI _declspec(dllexport)
#else
#define DLLAPI _declspec(dllimport)
#endif

#include <vector>
#include <string>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
DLLAPI  class Convert
{
private:
	std::vector<string> gtht { "-","+","0","1","2","3","4","5","6","7","8","9","A","a","B","b","C","c","D","d","E","e","F","f"};
public:
	enum Errors
	{
		Done, Error_In, Error_Out
	};
	Errors In_Out_int(string In, int& Out);
};
#define DLL_EXPORT