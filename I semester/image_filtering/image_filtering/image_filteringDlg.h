﻿
// image_filteringDlg.h: файл заголовка
//

#pragma once
#include <vector>
#include <math.h>
#include "GDIClass.h"
#include "Graf_Dialog.h"
using namespace std;

// Диалоговое окно CimagefilteringDlg
class CimagefilteringDlg : public CDialogEx
{
	// Создание
public:
	CimagefilteringDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IMAGE_FILTERING_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnBnClickedCheck2();
	afx_msg void OnBnClickedCheck3();
	afx_msg void OnBnClickedCheck4();
	BOOL midl_b;
	BOOL gauss_b;
	BOOL median_b;
	BOOL high_b;
	BOOL pic1_b;
	BOOL pic2_b;
	BOOL pic3_b;
	afx_msg void OnBnClickedCheck5();
	afx_msg void OnBnClickedCheck6();
	afx_msg void OnBnClickedCheck7();
	

	GDIClass gdi_obj1;
	GDIClass gdi_obj2;
	//для картинок
	//указатель на окно картинки
	CWnd* hWnd;
	//указатель контекст устройство
	CDC* hdc;
	//хранит размер окна
	CRect size;
	CDC* mdc;
	CBitmap* my_buffer;
	BOOL pic4_b;
	afx_msg void OnBnClickedCheck8();
	afx_msg void OnBnClickedButton2();
	int win_size;
	

	
	double noize_size;
	BOOL SWAP_PIC;
	afx_msg void OnBnClickedCheck9();
	BOOL square;
	BOOL rhombus;
	BOOL cross;
	afx_msg void OnBnClickedCheck10();
	afx_msg void OnBnClickedCheck11();
	afx_msg void OnBnClickedCheck12();
	afx_msg void OnBnClickedCheck13();
	Graf_Dialog gr_dialog; //Окно исследования
	afx_msg void OnBnClickedButton3(); //Кнопка запуска окна исследования
	void setup();
	BOOL noize_type;
	double error_size;
	double error_size_noize;
};
