﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется imagefiltering.rc
//
#define IDD_IMAGE_FILTERING_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_CHECK1                      1005
#define IDC_CHECK2                      1006
#define IDC_CHECK3                      1007
#define IDC_CHECK4                      1008
#define IDC_CHECK5                      1009
#define IDC_CHECK6                      1010
#define IDC_CHECK7                      1011
#define IDC_PIC1                        1012
#define IDC_PIC2                        1013
#define IDC_CHECK8                      1014
#define IDC_EDIT1                       1015
#define IDC_EDIT2                       1016
#define IDC_CHECK10                     1017
#define IDC_EDIT4                       1017
#define IDC_EDIT3                       1018
#define IDC_CHECK9                      1019
#define IDC_CHECK11                     1020
#define IDC_GRAFOUT                     1020
#define IDC_CHECK12                     1021
#define IDC_CHECK13                     1022
#define IDC_EDIT5                       1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
