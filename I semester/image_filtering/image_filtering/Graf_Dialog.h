﻿#pragma once
#include <vector>
#include "ChartViewer.h"
using namespace std;

// Диалоговое окно Graf_Dialog

class Graf_Dialog : public CDialogEx
{
	DECLARE_DYNAMIC(Graf_Dialog)

public:
	Graf_Dialog(CWnd* pParent = nullptr);   // стандартный конструктор
	virtual ~Graf_Dialog();

	// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	vector<vector<double>> picture_original;
	vector<vector<double>> picture_noize;
	vector<vector<double>> picture_new;
	int window_size;
	int shag; //отступ
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	void setup_midl(vector<vector<double>>& fil);
	void setup_gauss(vector<vector<double>>& fil);
	void setup_high(vector<vector<double>>& fil);
	void setup_VID(vector<vector<double>>& fil);
	void lin_filtering(vector<vector<double>>& data, vector<vector<double>>& fil);
	void med_filtering(vector<vector<double>>& data);
	CChartViewer viewer;
	void ViewerDraw(vector<double>& Datatime);
	void add_noizeGauss(vector<vector<double>>& Data, double noize_size);
	void add_noizeDrop(vector<vector<double>>& Data, double noize_size);
	double delta_error(vector<vector<double>>& Data1, vector<vector<double>>& Data2);
	DoubleArray vectorToArray(std::vector<double>& v)
	{
		return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
	}
	vector < vector<double>> studys;
	double noize_max;
	double dots;
	BOOL square;
	BOOL rhombus;
	BOOL cross;
	BOOL midl_b;
	BOOL gauss_b;
	BOOL median_b;
	BOOL high_b;

	void quickSort(vector <double>& arr, int left, int right)
	{
		int i = left, j = right;
		double tmp;
		double pivot = arr[(left + right) / 2];

		/* partition */
		while (i <= j) {
			while (arr[i] < pivot)
				i++;
			while (arr[j] > pivot)
				j--;
			if (i <= j) {
				tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
				i++;
				j--;
			}
		};

		/* recursion */
		if (left < j)
			quickSort(arr, left, j);
		if (i < right)
			quickSort(arr, i, right);

	}
	int win_size_min;
	int win_size_max;
	afx_msg void OnBnClickedButton2();
	BOOL noize_type;
};
