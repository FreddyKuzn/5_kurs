﻿
// image_filteringDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "image_filtering.h"
#include "image_filteringDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define GAUSS(x,y,x0,y0,sx,sy,ampl) ampl*exp(-(       ((x-x0)*(x-x0))/(sx*sx)     +   ((y-y0)*(y-y0))/(sy*sy)   ))  
// Диалоговое окно CimagefilteringDlg



CimagefilteringDlg::CimagefilteringDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IMAGE_FILTERING_DIALOG, pParent)
	, midl_b(FALSE)
	, gauss_b(FALSE)
	, median_b(FALSE)
	, high_b(FALSE)
	, pic1_b(FALSE)
	, pic2_b(FALSE)
	, pic3_b(FALSE)
	, pic4_b(FALSE)
	, win_size(5)
	, noize_size(0)
	, SWAP_PIC(FALSE)
	, square(FALSE)
	, rhombus(FALSE)
	, cross(FALSE)
	, noize_type(FALSE)
	, error_size(0)
	, error_size_noize(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CimagefilteringDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK1, midl_b);
	DDX_Check(pDX, IDC_CHECK2, gauss_b);
	DDX_Check(pDX, IDC_CHECK3, median_b);
	DDX_Check(pDX, IDC_CHECK4, high_b);
	DDX_Check(pDX, IDC_CHECK5, pic1_b);
	DDX_Check(pDX, IDC_CHECK6, pic2_b);
	DDX_Check(pDX, IDC_CHECK7, pic3_b);
	DDX_Check(pDX, IDC_CHECK8, pic4_b);
	DDX_Text(pDX, IDC_EDIT2, win_size);
	DDX_Control(pDX, IDC_PIC1, gdi_obj1); //ОТРИСОВКА GDI+
	DDX_Control(pDX, IDC_PIC2, gdi_obj2); //ОТРИСОВКА GDI+
	DDX_Text(pDX, IDC_EDIT3, noize_size);
	DDX_Check(pDX, IDC_CHECK9, SWAP_PIC);
	DDX_Check(pDX, IDC_CHECK10, square);
	DDX_Check(pDX, IDC_CHECK11, rhombus);
	DDX_Check(pDX, IDC_CHECK13, cross);
	DDX_Check(pDX, IDC_CHECK12, noize_type);
	DDX_Text(pDX, IDC_EDIT1, error_size);
	DDX_Text(pDX, IDC_EDIT5, error_size_noize);
}

BEGIN_MESSAGE_MAP(CimagefilteringDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CimagefilteringDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CimagefilteringDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_CHECK1, &CimagefilteringDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_CHECK2, &CimagefilteringDlg::OnBnClickedCheck2)
	ON_BN_CLICKED(IDC_CHECK3, &CimagefilteringDlg::OnBnClickedCheck3)
	ON_BN_CLICKED(IDC_CHECK4, &CimagefilteringDlg::OnBnClickedCheck4)
	ON_BN_CLICKED(IDC_CHECK5, &CimagefilteringDlg::OnBnClickedCheck5)
	ON_BN_CLICKED(IDC_CHECK6, &CimagefilteringDlg::OnBnClickedCheck6)
	ON_BN_CLICKED(IDC_CHECK7, &CimagefilteringDlg::OnBnClickedCheck7)
	ON_BN_CLICKED(IDC_CHECK8, &CimagefilteringDlg::OnBnClickedCheck8)
	ON_BN_CLICKED(IDC_BUTTON2, &CimagefilteringDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_CHECK9, &CimagefilteringDlg::OnBnClickedCheck9)
	ON_BN_CLICKED(IDC_CHECK10, &CimagefilteringDlg::OnBnClickedCheck10)
	ON_BN_CLICKED(IDC_CHECK11, &CimagefilteringDlg::OnBnClickedCheck11)
	//ON_BN_CLICKED(IDC_CHECK12, &CimagefilteringDlg::OnBnClickedCheck12)
	ON_BN_CLICKED(IDC_CHECK13, &CimagefilteringDlg::OnBnClickedCheck13)
	ON_BN_CLICKED(IDC_BUTTON3, &CimagefilteringDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// Обработчики сообщений CimagefilteringDlg

BOOL CimagefilteringDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию]
	UpdateData(1);
	pic1_b = 1;
	midl_b = 1;
	square = 1;
	UpdateData(0);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CimagefilteringDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CimagefilteringDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CimagefilteringDlg::OnBnClickedButton1()
{
	//Начальная установка
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	SWAP_PIC = 0;
	gr_dialog.picture_original.clear();
	gr_dialog.picture_noize.clear();
	vector<vector<double>> image;
	CImage img;
	if (pic1_b)img.Load(CString("520.bmp"));
	if (pic2_b)img.Load(CString("600.bmp"));
	if (pic3_b)img.Load(CString("1023.bmp"));
	if (pic4_b)img.Load(CString("100.bmp"));
	hWnd = GetDlgItem(IDC_PIC1);
	hdc = hWnd->GetDC();
	hWnd->GetClientRect(&size);
	CDC* mdc = new CDC();
	mdc->CreateCompatibleDC(hdc);

	CBitmap bmp_line;
	bmp_line.CreateCompatibleBitmap(hdc, size.Width(), size.Height());
	//сделали объект текущим
	CBitmap* tmp_line = mdc->SelectObject(&bmp_line);
	COLORREF color;
	int p = 200;
	gr_dialog.picture_original.resize(p);
	for (int i = 0; i < p; i++)
	{
		gr_dialog.picture_original[i].resize(p);
	}
	for (int i = 0; i < p; i++)
	{
		for (int j = 0; j < p; j++)
		{
			color = img.GetPixel(i, j);
			gr_dialog.picture_original[i][j] = (GetRValue(color) + GetGValue(color) + GetBValue(color)) / 3;
		}
	}
	gr_dialog.picture_noize = gr_dialog.picture_original;
	if(noize_type)gr_dialog.add_noizeGauss(gr_dialog.picture_noize, noize_size);
	else gr_dialog.add_noizeDrop(gr_dialog.picture_noize, noize_size);
	UpdateData(0);
	gdi_obj1.GradientCell(gr_dialog.picture_noize);
	gdi_obj1.MyPaint();
}
void CimagefilteringDlg::setup()
{
	gr_dialog.square= square;
	gr_dialog.rhombus=rhombus;
	gr_dialog.cross= cross;
	gr_dialog.midl_b= midl_b;
	gr_dialog.gauss_b= gauss_b;
	gr_dialog.median_b= median_b;
	gr_dialog.high_b= high_b;
	gr_dialog.window_size=win_size;
}
void CimagefilteringDlg::OnBnClickedButton2() //Фильтрация
{
	UpdateData(1);
	if (gr_dialog.picture_noize.size() == 0)return;
	if ((win_size % 2) == 0 || win_size <= 0)return;
	setup();
	vector<vector<double>> filtr;
	if (SWAP_PIC && gr_dialog.picture_new.size() != 0)
	{
		gr_dialog.picture_noize = gr_dialog.picture_new;
	}
	gr_dialog.picture_new.clear();
	gr_dialog.picture_new = gr_dialog.picture_noize;
	if (midl_b) gr_dialog.setup_midl(filtr);
	if (gauss_b) gr_dialog.setup_gauss(filtr);
	if (high_b) gr_dialog.setup_high(filtr);
	if (median_b)
	{
		gr_dialog.med_filtering(gr_dialog.picture_new);
	}
	else
	{
		gr_dialog.setup_VID(filtr);
		gr_dialog.lin_filtering(gr_dialog.picture_new, filtr);
	}
	error_size = gr_dialog.delta_error(gr_dialog.picture_new, gr_dialog.picture_original);
	error_size_noize = gr_dialog.delta_error(gr_dialog.picture_noize, gr_dialog.picture_original);
	gdi_obj2.GradientCell(gr_dialog.picture_new);
	gdi_obj2.MyPaint();
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedButton3()
{
	setup();
	gr_dialog.DoModal();
}


void CimagefilteringDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}




void CimagefilteringDlg::OnBnClickedCheck1()
{
	UpdateData(1);
	midl_b = 1;
	gauss_b = 0;
	median_b = 0;
	high_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck2()
{
	UpdateData(1);
	midl_b = 0;
	gauss_b = 1;
	median_b = 0;
	high_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck3()
{
	UpdateData(1);
	midl_b = 0;
	gauss_b = 0;
	median_b = 1;
	high_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck4()
{
	UpdateData(1);
	midl_b = 0;
	gauss_b = 0;
	median_b = 0;
	high_b = 1;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck5()
{
	UpdateData(1);
	pic1_b = 1;
	pic2_b = 0;
	pic3_b = 0;
	pic4_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck6()
{
	UpdateData(1);
	pic1_b = 0;
	pic2_b = 1;
	pic3_b = 0;
	pic4_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck7()
{
	UpdateData(1);
	pic1_b = 0;
	pic2_b = 0;
	pic3_b = 1;
	pic4_b = 0;
	UpdateData(0);
}
void CimagefilteringDlg::OnBnClickedCheck8()
{
	UpdateData(1);
	pic1_b = 0;
	pic2_b = 0;
	pic3_b = 0;
	pic4_b = 1;
	UpdateData(0);
}

void CimagefilteringDlg::OnBnClickedCheck9()
{
	UpdateData(1);
	if (!SWAP_PIC)OnBnClickedButton1();
	UpdateData(0);
}


void CimagefilteringDlg::OnBnClickedCheck10()
{
	UpdateData(1);
	square = 1;
	rhombus = 0;
	cross = 0;
	UpdateData(0);
}


void CimagefilteringDlg::OnBnClickedCheck11()
{
	UpdateData(1);
	square = 0;
	rhombus = 1;
	cross = 0;
	UpdateData(0);
}


void CimagefilteringDlg::OnBnClickedCheck12()
{
	UpdateData(1);
	square = 0;
	rhombus = 0;
	cross = 0;
	UpdateData(0);
}


void CimagefilteringDlg::OnBnClickedCheck13()
{
	UpdateData(1);
	square = 0;
	rhombus = 0;
	cross = 1;
	UpdateData(0);
}



