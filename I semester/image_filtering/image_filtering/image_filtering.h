﻿
// image_filtering.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CimagefilteringApp:
// Сведения о реализации этого класса: image_filtering.cpp
//

class CimagefilteringApp : public CWinApp
{
public:
	CimagefilteringApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CimagefilteringApp theApp;
