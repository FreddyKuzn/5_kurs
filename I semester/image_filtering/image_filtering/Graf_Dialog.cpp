﻿// Graf_Dialog.cpp: файл реализации
//
#pragma once
#include "pch.h"
#include "image_filtering.h"
#include "Graf_Dialog.h"
#include "afxdialogex.h"


#define GAUSS(x,y,x0,y0,sx,sy,ampl) ampl*exp(-(       ((x-x0)*(x-x0))/(sx*sx)     +   ((y-y0)*(y-y0))/(sy*sy)   ))  
// Диалоговое окно Graf_Dialog

IMPLEMENT_DYNAMIC(Graf_Dialog, CDialogEx)

Graf_Dialog::Graf_Dialog(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, noize_max(2)
	, dots(40)
	, win_size_min(3)
	, win_size_max(11)
	, noize_type(FALSE)
{

}

Graf_Dialog::~Graf_Dialog()
{
}

void Graf_Dialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRAFOUT, viewer); //ОТРИСОВКА GDI+
	DDX_Text(pDX, IDC_EDIT1, noize_max);
	DDX_Text(pDX, IDC_EDIT2, dots);
	DDX_Text(pDX, IDC_EDIT4, win_size_min);
	DDX_Text(pDX, IDC_EDIT3, win_size_max);
	DDX_Check(pDX, IDC_CHECK1, noize_type);
}


BEGIN_MESSAGE_MAP(Graf_Dialog, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &Graf_Dialog::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &Graf_Dialog::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &Graf_Dialog::OnBnClickedButton2)
END_MESSAGE_MAP()


BOOL Graf_Dialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	// TODO: добавьте дополнительную инициализацию]
	UpdateData(1);
	UpdateData(0);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}



void Graf_Dialog::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}


void Graf_Dialog::OnBnClickedButton1()
{
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	if (picture_original.size() == 0) return;
	shag = (window_size - 1) / 2;
	picture_noize.clear();
	studys.clear();
	studys.resize(4);
	double noize_step = noize_max / dots;
	for (double noize_lvl = 0; noize_lvl < noize_max; noize_lvl += noize_step)
	{
		picture_noize = picture_original;
		if(noize_type)add_noizeGauss(picture_noize, noize_lvl);
		else add_noizeDrop(picture_noize, noize_lvl);
		studys[0].push_back(delta_error(picture_noize, picture_original));
		///////////////////////////////////////////////////////////////////////////////////////////
		vector<vector<double>> filtr;
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square=1;
		rhombus=0;
		cross=0;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[1].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square = 0;
		rhombus = 1;
		cross = 0;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[2].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square = 0;
		rhombus = 0;
		cross = 1;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[3].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////

	}
	vector<double> Datatime;
	for (int i = 0; i < studys[0].size(); i++)Datatime.push_back(i * noize_step);
	ViewerDraw(Datatime);
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	UpdateData(0);
}
void Graf_Dialog::OnBnClickedButton2()
{
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	if (picture_original.size() == 0) return;
	vector<double> Datatime;
	picture_noize.clear();
	studys.clear();
	studys.resize(4);
	for (int win_lvl = win_size_min; win_lvl <= win_size_max; win_lvl+=2)
	{
		window_size = win_lvl;
		shag = (window_size - 1) / 2;
		picture_noize = picture_original;
		if (noize_type)add_noizeGauss(picture_noize, noize_max);
		else add_noizeDrop(picture_noize, noize_max);
		studys[0].push_back(delta_error(picture_noize, picture_original));
		///////////////////////////////////////////////////////////////////////////////////////////
		vector<vector<double>> filtr;
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square = 1;
		rhombus = 0;
		cross = 0;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[1].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square = 0;
		rhombus = 1;
		cross = 0;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[2].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////
		picture_new = picture_noize;
		if (midl_b) setup_midl(filtr);
		if (gauss_b) setup_gauss(filtr);
		if (high_b) setup_high(filtr);
		square = 0;
		rhombus = 0;
		cross = 1;
		if (median_b)
		{
			med_filtering(picture_new);
		}
		else
		{
			setup_VID(filtr);
			lin_filtering(picture_new, filtr);
		}
		studys[3].push_back(delta_error(picture_new, picture_original));
		////////////////////////////////////////////////////////////////////////////////////////////
		Datatime.push_back(win_lvl);
	}	
	ViewerDraw(Datatime);
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	UpdateData(0);
}

double Graf_Dialog::delta_error(vector<vector<double>>& Data1, vector<vector<double>>& Data2)
{
	double sum=0;
	for (int i = shag; i < Data1.size() - shag; i++)
	{
		for (int j = shag; j < Data1[i].size() - shag; j++)
		{
			sum += sqrt(pow((Data1[i][j] - Data2[i][j]), 2));
		}
	}
	sum /= Data1.size() - 2 * shag;
	sum /= Data1.size() - 2 * shag;
	return sum;
}
void Graf_Dialog::add_noizeDrop(vector<vector<double>>& Data, double noize_size)
{
	noize_size /= 100;
	int count = pow(Data.size(), 2) * noize_size;
	int i = 0;
	do
	{
		int x = rand() % (Data.size());
		int y = rand() % (Data.size());
		if (Data[x][y] == 0 )
		{
			Data[x][y] = 255; i++;
		}
		else if (Data[x][y] == 255)
		{
			Data[x][y] = 0; i++;
		}
		else
		{
			double rrr = rand() / RAND_MAX;
			if (rrr > 0.5) Data[x][y] = 255;
			else Data[x][y] = 0;
			i++;
		}
	} while (i < count);
}
void Graf_Dialog::add_noizeGauss(vector<vector<double>>& Data, double noize_size) //new
{
	vector<vector<double>> noize;
	noize.resize(Data.size());
	for (int i = 0; i < Data.size(); i++)
	{
		noize[i].resize(Data[i].size());
		for (int j = 0; j < noize[i].size(); j++)
		{
			noize[i][j] = 0;
		}
	}
	double alfa;
	double sum_signal = 0;
	double sum_shum = 0;
	double Eh;
	for (int i = 0; i < Data.size(); i++)
	{
		for (int j = 0; j < Data[i].size(); j++)
		{
			double M, ksi;
			M = rand() % 9 + 12;
			ksi = 0;
			for (int k = 0; k < M; k++)
			{
				ksi += -1 + 2 * rand() * 1. / RAND_MAX;
			}
			noize[i][j] = ksi / M;

			sum_signal += Data[i][j] * Data[i][j];
			sum_shum += noize[i][j] * noize[i][j];
		}
	}
	Eh = noize_size * sum_signal / 100;
	alfa = sqrt(Eh / sum_shum);
	for (int i = 0; i < Data.size(); i++)
	{
		for (int j = 0; j < Data[i].size(); j++)
		{
			Data[i][j] += alfa * noize[i][j];
		}
	}
}
void Graf_Dialog::ViewerDraw(vector<double> &Datatime)
{
	// In this example, we simply use random data for the 3 data series.
	if (studys.size() == 0)return;
	DoubleArray data_study_0 = vectorToArray(studys[0]);
	DoubleArray data_study_1 = vectorToArray(studys[1]);
	DoubleArray data_study_2 = vectorToArray(studys[2]);
	DoubleArray data_study_3 = vectorToArray(studys[3]);

	DoubleArray timeStamps = vectorToArray(Datatime);

	// Create a XYChart object of size 600 x 400 pixels
	XYChart* c = new XYChart(780, 320);

	// Add a title box using grey (0x555555) 20pt Arial font
	c->addTitle("", "arial.ttf", 20, 0x555555);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	c->setPlotArea(100, 80, 580, 170, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox* b = c->addLegend(70, 15, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	// Set axis label font to 12pt Arial
	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	c->xAxis()->setColors(Chart::TextColor, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::TextColor);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	c->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	c->xAxis()->setTickDensity(80);
	c->yAxis()->setTickDensity(40);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	c->yAxis()->setTitle("", "arial.ttf", 14, 0x555555);
	// Add a line layer to the chart with 3-pixel line width
	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);

	// Add 3 data series to the line layer
	layer->addDataSet(data_study_0, 0xADFF2F, "Initial error");
	layer->addDataSet(data_study_1, 0xFF8C00, "square_filtering error");
	layer->addDataSet(data_study_2, 0x00CED1, "rhombus_filtering error");
	layer->addDataSet(data_study_3, 0x800080, "cross_filtering error");

	// The x-coordinates for the line layer
	layer->setXData(timeStamps);
	viewer.setChart(c);
	delete c;
}
void Graf_Dialog::med_filtering(vector<vector<double>>& data)
{
	if (data.size() == 0)return;
	vector<vector<double>> data_new = data;
	int fil_centr = (window_size - 1) / 2;
	for (int i = fil_centr; i < (data.size() - fil_centr); i++)
	{
		for (int j = fil_centr; j < (data[i].size() - fil_centr); j++)
		{
			vector<double> buffer;
			///////////////////////////////////////////
			if (square)
			{
				for (int m = 0; m < window_size; m++)
				{
					for (int n = 0; n < window_size; n++)
					{
						buffer.push_back(data[i + m - fil_centr][j + n - fil_centr]);
					}
				}
			}
			if (rhombus)
			{
				for (int m = 0; m < window_size; m++)
				{
					int ost = abs(m - fil_centr);
					for (int n = 0; n < ost; n++)
						buffer.push_back(data[i + m - fil_centr][j + n - fil_centr]);
					for (int n = window_size - 1; n > window_size - ost - 1; n--)
						buffer.push_back(data[i + m - fil_centr][j + n - fil_centr]);
				}
			}
			if (cross)
			{
				for (int m = 0; m < window_size; m++)
				{
					for (int n = 0; n < window_size; n++)
					{
						if (m != fil_centr && n != fil_centr)
							buffer.push_back(data[i + m - fil_centr][j + n - fil_centr]);
					}
				}
			}
			///////////////////////////////////////////
			quickSort(buffer, 0, buffer.size() - 1);
			data_new[i][j] = buffer[(buffer.size() - 1) / 2];
		}
	}
	data = data_new;
}
void Graf_Dialog::lin_filtering(vector<vector<double>>& data, vector<vector<double>>& fil)
{
	if (data.size() == 0)return;
	if (fil.size() == 0)return;
	vector<vector<double>> data_new = data;
	int fil_centr = (fil.size() - 1) / 2;
	double koeff_sum = 0;
	for (int m = 0; m < fil.size(); m++)
	{
		for (int n = 0; n < fil[m].size(); n++)
		{
			koeff_sum += fil[m][n];
		}
	}
	koeff_sum = 1 / koeff_sum;
	for (int i = fil_centr; i < (data.size() - fil_centr); i++)
	{
		for (int j = fil_centr; j < (data[i].size() - fil_centr); j++)
		{
			data_new[i][j] = 0;
			for (int m = 0; m < fil.size(); m++)
			{
				for (int n = 0; n < fil[m].size(); n++)
				{
					data_new[i][j] += fil[m][n] * data[i + m - fil_centr][j + n - fil_centr] * koeff_sum;
				}
			}
		}
	}
	data = data_new;
}
void Graf_Dialog::setup_midl(vector<vector<double>>& fil)
{
	fil.resize(window_size);
	for (int i = 0; i < fil.size(); i++)
	{
		fil[i].resize(window_size);
		for (int j = 0; j < fil[i].size(); j++) fil[i][j] = 1.;
	}
}
void Graf_Dialog::setup_gauss(vector<vector<double>>& fil)
{
	fil.resize(window_size);
	int fil_centr = (fil.size() - 1) / 2;
	for (int i = 0; i < fil.size(); i++)
	{
		fil[i].resize(window_size);
		for (int j = 0; j < fil[i].size(); j++) fil[i][j] = GAUSS(i, j, fil_centr, fil_centr, 1, 1, 1);
	}
}

void Graf_Dialog::setup_high(vector<vector<double>>& fil)
{
	fil.resize(window_size);
	int fil_centr = (fil.size() - 1) / 2;
	for (int i = 0; i < fil.size(); i++)
	{
		fil[i].resize(window_size);
		for (int j = 0; j < fil[i].size(); j++)  fil[i][j] = -1;
	}
	fil[fil_centr][fil_centr] = window_size * window_size;
}
void Graf_Dialog::setup_VID(vector<vector<double>>& fil)
{
	if (square)
	{
		return;
	}
	if (rhombus)
	{
		int fil_centr = (fil.size() - 1) / 2;
		for (int i = 0; i < fil.size(); i++)
		{
			int ost = abs(i - fil_centr);
			for (int j = 0; j < ost; j++) fil[i][j] = 0;
			for (int j = fil.size() - 1; j > fil.size() - ost - 1; j--) fil[i][j] = 0;
		}
		return;
	}
	if (cross)
	{
		int fil_centr = (fil.size() - 1) / 2;
		for (int i = 0; i < fil.size(); i++)
		{
			for (int j = 0; j < fil[i].size(); j++)
			{
				if (i != fil_centr && j != fil_centr)
					fil[i][j] = 0;
			}

		}
		return;
	}
	return;
}

