/* 
 * File:   cGPU_fast_filtering.h
 * Author: roman
 *
 * Created on 22 �������� 2013 �., 14:38
 */

/**
 * @file cGPU_fast_filtering.h  �����, ����������� �������� �������������� 
 * ���������� ������� � ��������� ������� � �������������� ���������� CUDA
 */
#ifndef CGPU_FAST_FILTERING_H
#define	 CGPU_FAST_FILTERING_H
#include "cFast_filtering.h"
#include "device_launch_parameters.h"
#include <cuda_runtime_api.h>

/**
 * �����, ����������� �������� �������������� 
 * ���������� ������� � ��������� ������� � �������������� ���������� CUDA
 */
class cGPU_fast_filtering: public cFast_filtering {
public:
    
    /**
     * ����������� �� ���������
     */
    cGPU_fast_filtering();
    
    /**
     * �����������
     * @param input     ������� ������
     * @param filters   ���� ��������
     */
    cGPU_fast_filtering(const signal_buf &input,
                        const bank_buf &filters);
    
    /**
     * ����������
     */
    ~cGPU_fast_filtering();
    
    /**
     * ��������� �������� ����������
     * @param outputs   ������, � ������� ����� �������� �������� �������
     * @return          ������� ������ ��������
     */
    bool filter(bank_buf& outputs);
    
private:
    
    /**
   * ��������� ������������ ����� ��������� ����������
   * @param outputs   ������, � ������� ����� �������� �������� �������
   */
  bool Filter_Device(bank_buf &outputs);

};

#endif	/* CGPU_FAST_FILTERING_H */

