/* 
 * File:   cGPU_fast_filtering.cpp
 * Author: roman
 * 
 * Created on 22 �������� 2013 �., 14:38
 */

#include "cGPU_fast_filtering.h"

/**
 * ����������� �� ���������
 */
cGPU_fast_filtering::cGPU_fast_filtering()
{}


 /**
  * �����������
  * @param input     ������� ������
  * @param filters   ���� ��������
  */
cGPU_fast_filtering::cGPU_fast_filtering(       const signal_buf &input,
                                                const bank_buf &filters) 
{
    this->input_signal = input;
    this->bank_filters = filters;
}

/**
 * ����������
 */
cGPU_fast_filtering::~cGPU_fast_filtering()
{
	input_signal.clear();
	vector <complex <float> >().swap(input_signal); 

	bank_filters.clear();
	vector <vector <complex <float> > >().swap(bank_filters);
}

 /**
  * ��������� �������� ����������
  * @param outputs   ������, � ������� ����� �������� �������� �������
  * @return          ������� ������ ��������
  */
bool cGPU_fast_filtering::filter(bank_buf& outputs)
{
    if (!this->Filter_Device(outputs)) return false;
    return true;
}
