#pragma once

#include <cufft.h>
#include "cGPU_fast_filtering.h"
#include <thrust\device_vector.h>
#include "kernels.cuh"
#define BLOCK_SIZE_2D 16
#define BLOCK_LENGTH 2048

///////// Overlap-save method of fast convolution ///////////////////
bool cGPU_fast_filtering::Filter_Device(bank_buf &outputs)
{
   // Signal in device memory
	thrust::device_vector<cuComplex> signal_dev;
	try
	{
		signal_dev.assign(input_signal.size(), make_cuComplex(0.0, 0.0));
	}
	catch (thrust::system_error &err)
	{
		return false;
	}

	cudaError_t cuda_error;
	cuda_error = cudaMemcpy(	raw_pointer_cast(signal_dev.data()), 
								&input_signal[0], 
								input_signal.size() * sizeof(cuComplex), 
								cudaMemcpyHostToDevice);
	if (cuda_error != cudaSuccess) return false;

	// Size of bank filters, padded of BLOCK_SIZE_2D
	int padded_filters_size = bank_filters.size();
	if ((padded_filters_size % BLOCK_SIZE_2D)!=0)
	{
		padded_filters_size = (padded_filters_size / BLOCK_SIZE_2D + 1) * BLOCK_SIZE_2D;
	}
	
	// Filters in device memory (1d-vector)
	thrust::device_vector<cuComplex> filters_dev;
	
	try
	{
		filters_dev.assign(	BLOCK_LENGTH * padded_filters_size,
							make_cuComplex(0.0f, 0.0f));
	}
	catch (thrust::system_error &err)
	{
		return false;
	}

	// Zero padding vector in device memory
	thrust::device_vector<cuComplex> padding;
	// Max size of filters
	int max_filter_size = bank_filters[0].size();
	for (int i = 0; i < bank_filters.size(); i++)
	{
		if (bank_filters[i].size() > max_filter_size) 
			max_filter_size = bank_filters[i].size();
		
		// Copy filters from CPU memory to GPU memory
		cuda_error = cudaMemcpy(	raw_pointer_cast(filters_dev.data() + BLOCK_LENGTH * i), 
									&bank_filters[i][0],
									bank_filters[i].size() * sizeof(cuComplex),
									cudaMemcpyHostToDevice);
		
		if (cuda_error != cudaSuccess) return false;
	}

	// FFT of filters
	cufftHandle plan;
	cufftResult cufft_error;
	
	cufft_error = cufftPlan1d(	&plan, 
								BLOCK_LENGTH,
								CUFFT_C2C,
								bank_filters.size());
	if (cufft_error != CUFFT_SUCCESS) return false;

	cufft_error = cufftExecC2C(	plan, 
								thrust::raw_pointer_cast(filters_dev.data()), 
								thrust::raw_pointer_cast(filters_dev.data()), 
								CUFFT_FORWARD);
	if (cufft_error != CUFFT_SUCCESS) return false;

	// Current block of signal
	thrust::device_vector <cuComplex> current_section;
	// Count blocks in signal
	int count_blocks = input_signal.size() / (BLOCK_LENGTH - max_filter_size + 1);	
	if (input_signal.size() %  (BLOCK_LENGTH - max_filter_size + 1) != 0)
	{
		if ((input_signal.size() %  (BLOCK_LENGTH - max_filter_size + 1)) > BLOCK_LENGTH / 2) count_blocks++;

		try
		{
			signal_dev.resize(	count_blocks * (BLOCK_LENGTH - max_filter_size + 1), 
								make_cuComplex(0.0f, 0.0f));
		}
		catch (thrust::system_error &err)
		{
			return false;
		}
		
	}

	try
	{
		padding.clear();
		padding.assign(max_filter_size - 1, make_cuComplex(0.0f, 0.0f));
		signal_dev.insert(signal_dev.begin(), padding.begin(), padding.end());
	}
	catch (thrust::system_error &err)
	{
		return false;
	}
	
	// FFT plan of signal
	cufftHandle plan_signal;
	cufft_error = cufftPlan1d(	&plan_signal, 
								BLOCK_LENGTH,
								CUFFT_C2C,
								1);
	if (cufft_error != CUFFT_SUCCESS) return false;
	
	// CUDA blocks
	int blocksX = BLOCK_LENGTH / BLOCK_SIZE_2D;
	int blocksY = padded_filters_size / BLOCK_SIZE_2D;
	
	outputs.clear();
	outputs.assign(bank_filters.size(), signal_buf());
	for (int i = 0; i < bank_filters.size(); i++)
	{
		outputs[i].resize((BLOCK_LENGTH - max_filter_size + 1) * count_blocks);
	}

	thrust::device_vector <cuComplex> outputs_dev;

	// Loop for blocks in signal
	for (int i = 0; i < count_blocks; i++)
	{
		try
		{
			outputs_dev.assign(	filters_dev.begin(), 
								filters_dev.end());
			// Initialize current block of signal
			current_section.assign(	signal_dev.begin() + i * (BLOCK_LENGTH - max_filter_size + 1),
									signal_dev.begin() + i * (BLOCK_LENGTH - max_filter_size + 1) + BLOCK_LENGTH);
		}
		catch (thrust::system_error &err)
		{
			return false;
		}

		// FFT of current block
		cufft_error = cufftExecC2C(	plan_signal, 
									raw_pointer_cast(current_section.data()),
									raw_pointer_cast(current_section.data()), 
									CUFFT_FORWARD);

		// Multiplicate signal and filters in frequency domain
		Multiplicate_kernel <<< dim3(blocksX, blocksY), dim3(BLOCK_SIZE_2D, BLOCK_SIZE_2D) >>> 
			(	raw_pointer_cast(current_section.data()), 
				raw_pointer_cast(outputs_dev.data()),
				BLOCK_LENGTH,
				padded_filters_size);
		cuda_error = cudaGetLastError();
		if (cuda_error != cudaSuccess) return false;

		// Inverse FFT
		cufft_error = cufftExecC2C(	plan, 
									raw_pointer_cast(outputs_dev.data()), 
									raw_pointer_cast(outputs_dev.data()), 
									CUFFT_INVERSE);
		if (cufft_error != CUFFT_SUCCESS) return false;
		
		// Copy memory of output signals from the GPU to the CPU
		for (int j = 0; j < bank_filters.size(); j++)
		{
			//!!!!!!!!!!!!!
			cuda_error = cudaMemcpy(	&outputs[j][i * (BLOCK_LENGTH - max_filter_size + 1)], 
										raw_pointer_cast(outputs_dev.data()) + j * BLOCK_LENGTH,
										(BLOCK_LENGTH - max_filter_size + 1) * sizeof(cuComplex), 
										cudaMemcpyDeviceToHost);
			//!!!!!!!!!!!!!!
			// �� ���������
			if (cuda_error != cudaSuccess) return false;
		}
	}

	cufft_error = cufftDestroy(plan);
	if (cufft_error != CUFFT_SUCCESS) return false;

	cufft_error = cufftDestroy(plan_signal);
	if (cufft_error != CUFFT_SUCCESS) return false;

	//// Free GPU memory
	try
	{
		signal_dev.clear();
		thrust::device_vector<cuComplex>().swap(signal_dev);

		filters_dev.clear();
		thrust::device_vector<cuComplex>().swap(filters_dev);

		padding.clear();
		thrust::device_vector<cuComplex>().swap(padding);

		current_section.clear();
		thrust::device_vector<cuComplex>().swap(current_section);

		outputs_dev.clear();
		thrust::device_vector<cuComplex>().swap(outputs_dev);
	}
	catch (thrust::system_error &err)
	{
		return false;
	}

	return true;
}