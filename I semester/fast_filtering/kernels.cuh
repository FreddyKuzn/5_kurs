#pragma once
#include "device_launch_parameters.h"
#include <cuda_runtime_api.h>
#include <cufft.h>
#include <cublas_v2.h>


extern "C" __global__ void Multiplicate_kernel(	cuComplex *input_a,
											cuComplex *input_b, 
											int width,
											int height);

