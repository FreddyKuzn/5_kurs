/* 
 * File:   cFast_filtering.h
 * Author: roman
 *
 * Created on 21 ���� 2013 �., 17:45
 */

#ifndef CFAST_FILTERING_H
#define	CFAST_FILTERING_H

/**
 * @file cFast_filtering.h ����������� ����� ��������� �������������� ����������
 */

#include <vector>
#include <complex>
using namespace std;

/**
 * ����������� ����� ��������� �������������� ����������
 */
class cFast_filtering
{
protected:

	typedef vector < complex < float > > signal_buf;
	typedef vector < vector < complex < float > > > bank_buf;

    /** ������� ������ */
    vector < complex < float > > input_signal;
    
    /** ���� �������� */
    vector < vector < complex < float > > >  bank_filters;
    
public:
    /**
     * ��������� �������� ���������� (����� ����������� �����)
     * @param outputs   ������, � ������� ����� �������� �������� �������
     * @return          ������� ������ ��������
     */
    virtual bool filter(bank_buf &outputs) = 0;
    
    /**
     * ����������� ����������
     */
    virtual ~cFast_filtering(){}
};

#endif	/* CFAST_FILTERING_H */

