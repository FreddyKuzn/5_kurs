/* 
 * File:   cSimple_convolution.h
 * Author: roman
 *
 * Created on 30 ���� 2013 �., 11:27
 */

#ifndef CSIMPLE_CONVOLUTION_H
#define	CSIMPLE_CONVOLUTION_H

/**
 * @file cSimple_convolution.h  �����, ����������� �������� ������� �������������� ������
 */

#include "cFast_filtering.h"

/**
 * �����, ����������� �������� ������� �������������� ������ (��� �����������)
 */
class cSimple_convolution : public cFast_filtering {
 
public:

	static void fft(signal_buf &buffer, int is);
        
    /**
     * ����������� �� ���������
     */
    cSimple_convolution();
    
    /**
     * �����������
     * @param input     ������� ������
     * @param filters   ���� ��������
     */
    cSimple_convolution(       const signal_buf &input,
                               const bank_buf &filters);
    
    /**
     * ����������
     */
    ~cSimple_convolution();
    
    /**
     * ��������� �������� ����������
     * @param outputs   ������, � ������� ����� �������� �������� �������
     * @return          ������� ������ ��������
     */
    bool filter(bank_buf &outputs);   
};

#endif	/* CSIMPLE_CONVOLUTION_H */

