/* 
 * File:   cTbb_fast_filtering.cpp
 * Author: roman
 * 
 * Created on 21 ���� 2013 �., 17:44
 */

#include "cTbb_fast_filtering.h"
#include <iostream>
#include <fstream>
#include <complex>

using namespace tbb;
using namespace std;

#define BLOCK_LENGTH 2048

/**
* ����������� �� ���������
*/
cTbb_fast_filtering::cTbb_fast_filtering() {
}

/**
* �����������
* @param input     ������� ������
* @param filters   ���� ��������
*/
cTbb_fast_filtering::cTbb_fast_filtering(       const signal_buf &input,
                                                const bank_buf &filters)
{
        ippInit();
        input_signal = input;
        bank_filters = filters;
}

/**
* ����������
*/
cTbb_fast_filtering::~cTbb_fast_filtering() {
}

  /**
    * �����������
    * @param filter_input      ������� ������
    * @param fft_spec          ���� �������������� �����
    * @param direction         ����������� ��������������
    */
cTbb_fast_filtering::cFFT_task::cFFT_task(      Ipp32fc* filter_input,
                                                IppsFFTSpec_C_32fc* fft_spec,
                                                bool direction):
                                                filter(filter_input),
                                                spec(fft_spec),
                                                dir(direction)
{}

 /**
* ��������� ������
* @return ��������� �� ��������� ������
*/
task* cTbb_fast_filtering::cFFT_task::execute()
{
    //��������� ������� �������������� ����� � ������ �����������
    // ��������� �������������� ����� �������� � ��� �� ������
    if (dir)
    {
        ippsFFTFwd_CToC_32fc_I(filter, spec, NULL);
    }
    else
    {
        ippsFFTInv_CToC_32fc_I(filter, spec, NULL);
    }
    return NULL;
}

 /**
    *  ���������� ������ ������ �� ��������, ������ ������� ����� 2
    *  @return         ������� ������ ��������
    */
bool cTbb_fast_filtering::pad_data()
{
    // ��������� ������� �������
    unsigned long size_signal = input_signal.size();
    
    // ��� ������� ����� ������� �������� ��������� ����������
    if (size_signal == 0) return false;
    
      // ��������� ���������� ������� � �����
    unsigned int count_filters = bank_filters.size();
    
    // ��� ������� ���������� �������� �������� ��������� ����������
    if (count_filters == 0) return false;

	max_filter_size = bank_filters[0].size();
      
    // � ����� �� ��������
    for (unsigned int i = 0; i < count_filters; i++)
    {
        // ���� ����� ������� �������, �� �������� ��������� ����������
        if (bank_filters[i].size() == 0) return false;
        
        // ���� ����� ������� ������ ����� �������, �������� ��������� ����������
        if (bank_filters[i].size() >= size_signal) return false;

		if (bank_filters[i].size() > max_filter_size) max_filter_size = bank_filters[i].size();
        
        // ��������� ������ �� ������� ��������������� �����
        bank_filters[i].resize(BLOCK_LENGTH, complex<float>(0.0f, 0.0f));
    }
	  // ������ ������������ �������
    unsigned long size_signal_padded = size_signal;
    
    // ���� ������ ������� �� ������ ������� ��������������� �����
    if ((size_signal % (BLOCK_LENGTH - max_filter_size + 1)) != 0)
	{
		if ((size_signal % (BLOCK_LENGTH - max_filter_size + 1)) < BLOCK_LENGTH / 2)
		size_signal_padded = (size_signal / (BLOCK_LENGTH - max_filter_size + 1)) * (BLOCK_LENGTH - max_filter_size + 1);

		else
		size_signal_padded = (size_signal / (BLOCK_LENGTH - max_filter_size + 1) + 1) * (BLOCK_LENGTH - max_filter_size + 1);
	}
    
    // ��������� ������ ������ �� ������������ �������
    input_signal.resize(size_signal_padded, complex < float > (0., 0));
      
	signal_buf padding(max_filter_size - 1, complex <float> (0.0f, 0.0f));
	input_signal.insert(input_signal.begin(), padding.begin(), padding.end());

    return true;
}

/**
* ������� �� ������� �������� ������ � �������� Intel IPP
* @param status    ������������ �������� ������� Intel IPP
* @return          ������� ������� ������
*/
bool cTbb_fast_filtering::error_description(IppStatus status)
{
    bool success = true;
    // ���� � ������� �� ���������� �������� ������, ���������� true
    // � ��������� ������ �������� �������� �� �������, ������� ��� �� ������� � ���������� false
    if (status != ippStsNoErr)
    {
        cout<<ippGetStatusString(status)<<endl;
        success = false;
    }
    
    return success;
}

/*void cTbb_fast_filtering::cCopy_functor::operator ()
                        (const blocked_range<unsigned long>& r) const
{
    for (unsigned long i = r.begin(); i != r.end(); i++)
    {
        signal_copy[i].re = input_signal[i].real();
        signal_copy[i].im = input_signal[i].imag();
    }
}

void cTbb_fast_filtering::cCopy_filters_functor::operator ()
        (const blocked_range2d<unsigned int,unsigned long>& r) const
{
     // ������� ���� �� ��������
    for (unsigned int i = r.rows().begin(); i != r.rows().end(); i++)
    {
        // ���������� ���� �� ������������� �������
        for (unsigned long j = r.cols().begin(); j != r.cols().end(); j++)
        {
            bank_filters_copy[i][j].re = bank_filters[i][j];
            bank_filters_copy[i][j].im = 0;
        }
    }
}*/

  /**
    * ��������� �������� ����������
    * @param outputs   ������, � ������� ����� �������� �������� �������
    * @return          ������� ������ ��������
    */
bool cTbb_fast_filtering::filter(bank_buf &outputs)
{
    // ��������� ������� � �������
    if (!pad_data()) return false; 
    
    // ������������� ���������� Intel TBB 
    task_scheduler_init init;
   
    // ������ ������� Intel IPP
    IppStatus status;
    // ���� �������������� ����� �������
    IppsFFTSpec_C_32fc *fft_spec;
    
    // ��������� ������� �������� �������
    unsigned long signal_size = input_signal.size();

	/** ��������� ������ */
	int order = log((double)BLOCK_LENGTH) / log(2.0);
	int pSpecSize = 0, pSpecBufferSize = 0, pBufferSize = 0;
	status = ippsFFTGetSize_C_32fc(order,
		IPP_FFT_DIV_FWD_BY_N,
		ippAlgHintNone,
		&pSpecSize,
		&pSpecBufferSize,
		&pBufferSize);

	// � ������ ������ �������� ��������� ����������
	if (!error_description(status)) return false;
	
	Ipp8u *pFFTSpecBuf = ippsMalloc_8u(pSpecSize);
	Ipp8u *pFFTInitBuf = ippsMalloc_8u(pSpecBufferSize);
	Ipp8u *pFFTWorkBuf = ippsMalloc_8u(pBufferSize);

	// �������� ����� �������������� �����
	status = ippsFFTInit_C_32fc(&fft_spec,
		order,
		IPP_FFT_DIV_FWD_BY_N,
		ippAlgHintNone,
		pFFTSpecBuf,
		pFFTInitBuf);
    
  
      // � ������ ������ �������� ��������� ����������
    if (!error_description(status)) return false;
    
    // ������ ���� Ipp32fc �������� ��������������� �����, � ������� ���������� �����������
    // ������� ������ ��� ���������� ������ � ���
    Ipp32fc *signal_copy = new Ipp32fc[BLOCK_LENGTH];
    
    // ������ ������ affinity_partitioner ��� ������������� ������������� ������������ ����� ��������
    // � �������� tbb::parallel_for
    affinity_partitioner partitioner;

	 // ��������� ���������� ��������
    unsigned int count_filters = bank_filters.size();
   
	// ������ ���� Ipp32fc, � ������ ����������� ����������� ���� ��������
    // ��� ���������� ������ � ����
    Ipp32fc **bank_filters_copy = new Ipp32fc* [count_filters];
	Ipp32fc **spectrum = new Ipp32fc* [count_filters];
    
    // ��������� ������ ��� ������ ������
    for (unsigned i = 0; i < count_filters; i++)
    {
        bank_filters_copy[i] = new Ipp32fc [BLOCK_LENGTH];
		spectrum[i] = new Ipp32fc[BLOCK_LENGTH];
    }
    
    // ��������� ������������ ����������� ���������
    parallel_for(blocked_range < unsigned int > 
            (0, count_filters), 
            [&] (const blocked_range < unsigned int > &r)
			{
				// ������� ���� �� ��������
				for (unsigned int i = r.begin(); i != r.end(); i++)
				{
					memcpy(bank_filters_copy[i], &bank_filters[i][0], BLOCK_LENGTH * sizeof(Ipp32fc));
				}
			},
            partitioner);
   
     // ������ ���������� ����� �������������� ����� ��� ������ ��������
    task_list fft_multiple;
    // ������ ������ �������������� ��� ���������
    vector <IppsFFTSpec_C_32fc*> fft_filter_plans(count_filters, NULL);
	status = ippsFFTGetSize_C_32fc(order,
		IPP_FFT_DIV_FWD_BY_N,
		ippAlgHintNone,
		&pSpecSize,
		&pSpecBufferSize,
		&pBufferSize);


	pFFTSpecBuf = ippsMalloc_8u(pSpecSize);
	pFFTInitBuf = ippsMalloc_8u(pSpecBufferSize);
	pFFTWorkBuf = ippsMalloc_8u(pBufferSize);

	 // ���� �� ��������
    for (unsigned int i = 0; i != count_filters; i++)
    {
        // ������������� ����� �������������� ��� �������� �������
		status = ippsFFTInit_C_32fc(	&fft_filter_plans[i],
									order,
									IPP_FFT_DIV_FWD_BY_N,
									ippAlgHintNone,
									pFFTSpecBuf,
									pFFTInitBuf);
        // � ������ ������ �������� ��������� ����������
        if (!error_description(status)) return false;
        
        // �������� ����� ���������� ������ � ��������� � � ������
       fft_multiple.push_back(*new (task::allocate_root()) 
        cFFT_task(bank_filters_copy[i], fft_filter_plans[i], true));
    }
    
    // ������������ ���������� ������ ����� - ��� ��� ������ ��������
    task::spawn_root_and_wait(fft_multiple);
 /*   parallel_for(blocked_range<unsigned>(0, count_filters), [&]
        (const blocked_range<unsigned> &r)
        {
                for (unsigned i = r.begin(); i != r.end(); i++)
                {
                    ippsFFTFwd_CToC_32fc_I(bank_filters_copy[i], fft_filter_plans[i], NULL);
                }
        },
        partitioner);*/

	int blocks = signal_size / (BLOCK_LENGTH - max_filter_size + 1);
	unsigned long index_begin;

	// ������� ������ �������� �������
	outputs.clear();
	outputs.assign(count_filters, vector < complex < float > > ());
	for (int i = 0; i < count_filters; i++)
	{
		outputs[i].resize((BLOCK_LENGTH - max_filter_size + 1) * blocks);
	}
    
	// ���� �� ������ � �������
	for (int i_block = 0; i_block < blocks; i_block++)
	{
		index_begin = i_block*(BLOCK_LENGTH - max_filter_size + 1);
		 // ��������� ������������ ����������� �������� ������� � ������� �������������� ����
		/*parallel_for(blocked_range<unsigned long> (0, BLOCK_LENGTH), 
            [&](const blocked_range<unsigned long> &r)
			{
				for (unsigned long i = r.begin(); i != r.end(); i++)
				{
					signal_copy[i].re = input_signal[index_begin + i].real();
					signal_copy[i].im = input_signal[index_begin + i].imag();
				}
			},  
			partitioner);*/
		memcpy(signal_copy, &input_signal[index_begin], BLOCK_LENGTH * sizeof(Ipp32fc));

		// ��������� ��� ��� ��������
		status = ippsFFTFwd_CToC_32fc_I(signal_copy, fft_spec, NULL);

		 // � ������ ������ �������� ��������� ����������
		if (!error_description(status)) return false;

		// ������������ ������������ ������� ������� �� ������������ ��������������
		 // ������� �������
		parallel_for(
			blocked_range2d<unsigned int, unsigned long> (0, count_filters, 0, BLOCK_LENGTH), [&]
            (const blocked_range2d <unsigned int, unsigned long> &r)
            {
                // ������� ���� �� ��������
                for (unsigned int i = r.rows().begin(); i != r.rows().end(); i++)
                {
                    // ���������� ���� �� ��������� �������
                    for (unsigned long j = r.cols().begin(); j != r.cols().end(); j++)
                    {
						spectrum[i][j].re =
							signal_copy[j].re;/* *bank_filters_copy[i][j].re -
                                signal_copy[j].im * bank_filters_copy[i][j].im;*/
						spectrum[i][j].im =
							signal_copy[j].im; /**bank_filters_copy[i][j].im +
                                signal_copy[j].im * bank_filters_copy[i][j].re;*/
                    }
                }
            });

		 // ������� ������ �����       
		 fft_multiple.clear();
     
		 // � ��������� ��� ������ - ��� ��������� �������������� ����� 
		 for (unsigned int i = 0; i != count_filters; i++)
		 {
			  fft_multiple.push_back(*new (task::allocate_root()) 
					 cFFT_task(spectrum[i], fft_filter_plans[i], false));
		 }
     
		 // ������������ �������� ��� ��� ������ �������� ��������� �������
		 task::spawn_root_and_wait(fft_multiple);

	 	 // ��������� �������� ������
		 for (unsigned int i = 0; i != count_filters; i++)
		 {
			 // �������� �������� ������ � ������ �� �������
			 memcpy(&outputs[i][i_block * (BLOCK_LENGTH - max_filter_size + 1)], 
						&spectrum[i][max_filter_size - 1] ,
						(BLOCK_LENGTH - max_filter_size + 1) * sizeof(Ipp32fc));
		 }
     
	}    
            
     
            
     /* parallel_for(blocked_range<unsigned>(0, count_filters), [&]
        (const blocked_range<unsigned> &r)
        {
                for (unsigned i = r.begin(); i != r.end(); i++)
                {
                    ippsFFTInv_CToC_32fc_I(bank_filters_copy[i], fft_filter_plans[i], NULL);
                }
        },
        partitioner);*/

    
     // ������� ������
     delete [] signal_copy;
     delete [] bank_filters_copy;
	 if (pFFTWorkBuf) ippFree(pFFTWorkBuf);
	 if (pFFTSpecBuf) ippFree(pFFTSpecBuf);
	 if (pFFTInitBuf) ippFree(pFFTInitBuf);
    
     return true;
}
