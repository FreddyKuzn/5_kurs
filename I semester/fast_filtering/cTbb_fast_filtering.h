/* 
 * File:   cTbb_fast_filtering.h
 * Author: roman
 *
 * Created on 21 ���� 2013 �., 17:44
 */

/**
 * @file cTbb_fast_filtering.h  �����, ����������� �������� �������������� 
 * ���������� ������� � ��������� ������� � �������������� ���������� Intel TBB � Intel IPP
 */
#ifndef CTBB_FAST_FILTERING_H
#define	CTBB_FAST_FILTERING_H
#include "cFast_filtering.h"
#include <tbb/task.h>
#include <ipp.h>
#include <ipps.h>
#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range2d.h>
using namespace tbb;

/**
 * �����, ����������� �������� �������������� 
 * ���������� ������� � ��������� ������� � �������������� ���������� Intel TBB � Intel IPP
 */
class cTbb_fast_filtering: public cFast_filtering {
public:
    
    /**
     * ����������� �� ���������
     */
    cTbb_fast_filtering();
    
    /**
     * �����������
     * @param input     ������� ������
     * @param filters   ���� ��������
     */
    cTbb_fast_filtering(const signal_buf &input,
                        const bank_buf &filters);
    
    /**
     * ����������
     */
    ~cTbb_fast_filtering();
    
    /**
     * ��������� �������� ����������
     * @param outputs   ������, � ������� ����� �������� �������� �������
     * @return          ������� ������ ��������
     */
    bool filter(bank_buf &outputs);
    
    
private:
    
	unsigned long max_filter_size;

    /**
     *  ���������� ������ ������ �� ��������, ������ ������� ����� 2
     *  @return         ������� ������ ��������
     */
    bool pad_data();
    
    /**
     * ������� �� ������� �������� ������ � �������� Intel IPP
     * @param status    ������������ �������� ������� Intel IPP
     * @return          ������� ������� ������
     */
    bool error_description(IppStatus status);
    
   /* class cCopy_functor
    {
        vector < complex < float > > input_signal;
        Ipp32fc *signal_copy;
    public:
        void operator () (const blocked_range <unsigned long> &r) const;
        cCopy_functor(  vector < complex < float > > &input_signal_im,
                        Ipp32fc *signal_copy_im):
                        input_signal(input_signal_im),
                        signal_copy(signal_copy_im) 
        {}
    };
    
    class cCopy_filters_functor
    {
        vector < vector < float > > bank_filters;
        Ipp32fc **bank_filters_copy;
    public:
        void operator () (const blocked_range2d <unsigned int, unsigned long> &r) const;
        cCopy_filters_functor(  vector < vector < float> > &bank_filters_im,
                                Ipp32fc **bank_filters_copy_im):
                                bank_filters(bank_filters_im),
                                bank_filters_copy(bank_filters_copy_im)
        {}
    };*/

    /**
     *  ����� ���������� ������ TBB - ������� �������������� �����
     */
    class cFFT_task : public task
    {
        /** ������ (������, ��� ������� ����� ������������ ��������������) */
        Ipp32fc *filter;
        /** ���� �������������� ����� */
        IppsFFTSpec_C_32fc *spec;
        /** ����������� �������������� (true - ������, false - ��������) */
        bool dir;
        
        public:
            
            /**
             * ��������� ������
             * @return ��������� �� ��������� ������
             */
            task* execute();
            
            /**
             * �����������
             * @param filter_input      ������� ������
             * @param fft_spec          ���� �������������� �����
             * @param direction         ����������� ��������������
             */
            cFFT_task(  Ipp32fc *filter_input,
                        IppsFFTSpec_C_32fc *fft_spec,
                        bool direction);
    };
};

#endif	/* CTBB_FAST_FILTERING_H */

