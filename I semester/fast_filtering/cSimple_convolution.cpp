/* 
 * File:   cSimple_convolution.cpp
 * Author: roman
 * 
 * Created on 30 ���� 2013 �., 11:27
 */

#include "cSimple_convolution.h"
#include <iostream>
using namespace std;

// Block length
#define BLOCK_LENGTH 2048


/**
 * ����������� �� ���������
 */
cSimple_convolution::cSimple_convolution()
{
}

    /**
     * �����������
     * @param input     ������� ������
     * @param filters   ���� ��������
     */
cSimple_convolution::cSimple_convolution(  const signal_buf &input,
                                           const bank_buf &filters) {
    
    input_signal = input;
    bank_filters = filters;
}

 /**
* ����������
*/
cSimple_convolution::~cSimple_convolution() {
}

// Fast Fourier Transform
void cSimple_convolution::fft(signal_buf &buffer, int is)
{
	int i,j,istep;
	int m,mmax;
	float r,r1,theta,w_r,w_i,temp_r,temp_i;
	float pi=3.1415926f;
	int n = buffer.size();

	r=pi*is;
	j=0;
	for(i=0;i<n;i++)
	{
		if(i<j)
		{
			temp_r=buffer[j].real();
			temp_i=buffer[j].imag();
			buffer[j] = std::complex<float> (buffer[i].real(), buffer[i].imag());
			buffer[i] = std::complex<float> (temp_r, temp_i);
		}
		m=n>>1;
		while(j>=m) { j-=m; m=(m+1)/2; }
		j+=m;
	}
	mmax=1;
	while(mmax<n)
	{
		istep=mmax<<1;
		r1=r/(float)mmax;
		for(m=0;m<mmax;m++)
		{
			theta=r1*m;
			w_r=cos(theta);
			w_i=sin(theta);
			for(i=m;i<n;i+=istep)
			{
				j=i+mmax;
				temp_r=w_r*buffer[j].real() - w_i*buffer[j].imag();
				temp_i=w_r*buffer[j].imag() + w_i*buffer[j].real();
				buffer[j]=complex<float>(buffer[i].real() - temp_r, buffer[i].imag() - temp_i);
				buffer[i]+=complex<float>(temp_r, temp_i);
			}
		}
	mmax=istep;
	}

	for (int i = 0; i < n; i++)
	{
		buffer[i] /= (float)n;
	}
}

 /**
* ��������� �������� ����������
* @param outputs   ������, � ������� ����� �������� �������� �������
* @return          ������� ������ ��������
*/
bool cSimple_convolution::filter(vector < vector < complex < float > > > &outputs)
{
	 // ��� ������� ����� ������� ��� ������� ���������� �������� 
    // �������� ��������� ����������
    if (input_signal.size() == 0) return false;
    if (bank_filters.size() == 0) return false;
    
	// Section block length
	int block_length = 2048;

	// Count of blocks in signal
	int n_blocks;

	// Padding vector
	signal_buf padding;
	// Filters padded by zeros
	bank_buf padded_filters = bank_filters;
	// Current section of signals by Length = block_length
	signal_buf current_section(block_length, complex<float>(0.0f, 0.0f));
	// output signals
	outputs.clear();
	// Single output signal
	signal_buf single_output;
	// Signal padded by zeros
	signal_buf padded_signal = input_signal;

	unsigned max_filter_size = bank_filters[0].size();
	for (unsigned i = 0; i < bank_filters.size(); i++)
	{
		if (bank_filters[i].size() > max_filter_size) max_filter_size = bank_filters[i].size();
	}
	
	n_blocks = input_signal.size() / (block_length - max_filter_size + 1);
	padding.clear();
	// Padded signal is equals to signal
	padded_signal = input_signal;
	// Initialization padding vector
	padding.assign(max_filter_size-1, complex<float>(0.0f, 0.0f));
	// Zero padding left
	padded_signal.insert(padded_signal.begin(), padding.begin(), padding.end());

	// Loop for filters
	for (unsigned i = 0; i < bank_filters.size(); i++)
	{
		
		// Single output
		single_output.clear();
		// Filter zero padding
		padding.resize(block_length - bank_filters[i].size(), std::complex < float > (0.0f, 0.0f));
		padded_filters[i].insert(padded_filters[i].end(),padding.begin(), padding.end());
		
		// FFT of filter
		fft(padded_filters[i], -1);
		
		padding.clear();

		// Loop for blocks in signal
		for (int j = 0; j < n_blocks; j++)
		{
			// Initialize current block of signal
			memcpy(&current_section[0], &padded_signal[0] + j * (block_length - bank_filters[i].size() + 1) ,
				block_length * sizeof(std::complex<float>));
			//-------------------------------------------------------------------------------------------------
			// FFT of current block
			fft(current_section, -1);

			// Multiplication of signal and filter in frequency domain
			for (int k = 0; k < block_length; k++)
			{
				current_section[k] *= padded_filters[i][k];
			}

			// Inverse FFT
			fft(current_section, 1);

			// Save result in single_output array and discard M-1 points
			single_output.insert(single_output.end(), current_section.begin() + bank_filters[i].size() - 1, current_section.end());
		}

		outputs.push_back(single_output);
	}

    return true;
}
