#include "kernels.cuh"

__host__ __device__ cuComplex operator * (cuComplex a, cuComplex b)
{
	return make_cuComplex(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}


__global__ void Multiplicate_kernel(	cuComplex *input_a,
										cuComplex *input_b, 
										int width,
										int height)
{
    int ind_x = blockIdx.x * blockDim.x + threadIdx.x;
    int ind_y = blockIdx.y * blockDim.y + threadIdx.y;

    if ((ind_x < width)&&(ind_y < height))
    {
    	cuComplex temp = input_a[ind_x] * input_b[ind_y * width + ind_x];

		input_b[ind_y * width + ind_x].x = temp.x/width;	
		input_b[ind_y * width + ind_x].y = temp.y/width;
    }
}
