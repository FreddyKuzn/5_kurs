// fast_convolution.cpp : Defines the exported functions for the DLL application.
//


#include <vector>
#include <complex>
#define FROMDLL
#include "fast_convolution.h"
#include "cSimple_convolution.h"
#include "cGPU_fast_filtering.h"
//#include "cTbb_fast_filtering.h"
#include <fstream>
#include <algorithm>
using namespace std;

#define PI 3.1415926535897932384626433832795

/** Fast_convolustion function
 *  @param signal	Input signal
 *  @param filters	Bank of filters
 *  @param outputs  Output signals
 *  @param alg		Algorithm
 */
void __stdcall fast_convolution(	const signal_buf &signal,
									const bank_buf &filters,
									bank_buf &outputs,
									Filtering_Algorithm alg)
{
	// Object of algorithm
	cFast_filtering *algorithm = NULL;

	// Choose a constructor
	switch(alg)
	{
		case CPU_FD:
		{
			algorithm = new cSimple_convolution(signal, filters);
			break;
		}
		case GPU_FD:
		{
			algorithm = new cGPU_fast_filtering(signal, filters);
			break;
		}
		case CPU_TD:
		{
		//	algorithm = new cTbb_fast_filtering(signal, filters);
			break;
		}
	}

	// Filtration
	algorithm->filter(outputs);
	// Delete algorithm
	delete algorithm;
}

// Fast Fourier Transform
void fft(signal_buf &buffer, int is)
{
	int i,j,istep;
	int m,mmax;
	float r,r1,theta,w_r,w_i,temp_r,temp_i;
	float pi=3.1415926f;
	int n = buffer.size();

	r=pi*is;
	j=0;
	for(i=0;i<n;i++)
	{
		if(i<j)
		{
			temp_r=buffer[j].real();
			temp_i=buffer[j].imag();
			buffer[j] = std::complex<float> (buffer[i].real(), buffer[i].imag());
			buffer[i] = std::complex<float> (temp_r, temp_i);
		}
		m=n>>1;
		while(j>=m) { j-=m; m=(m+1)/2; }
		j+=m;
	}
	mmax=1;
	while(mmax<n)
	{
		istep=mmax<<1;
		r1=r/(float)mmax;
		for(m=0;m<mmax;m++)
		{
			theta=r1*m;
			w_r=cos(theta);
			w_i=sin(theta);
			for(i=m;i<n;i+=istep)
			{
				j=i+mmax;
				temp_r=w_r*buffer[j].real() - w_i*buffer[j].imag();
				temp_i=w_r*buffer[j].imag() + w_i*buffer[j].real();
				buffer[j]=complex<float>(buffer[i].real() - temp_r, buffer[i].imag() - temp_i);
				buffer[i]+=complex<float>(temp_r, temp_i);
			}
		}
	mmax=istep;
	}

	for (int i = 0; i < n; i++)
	{
		buffer[i] /= (float)n;
	}
}

/**
  * ��������� ������ �� �����
  * @param file_name ��� �����
  * @param filter    ������, � ������� ����� �������� ������������ �������
  * @return          ������� ������ ��������
  */
bool __stdcall LoadFilter(const std::string &file_name, vector < complex <float> > &filter)
{
	int strlen = file_name.length();
	char *name = new char[strlen + 1];
	for (int i = 0; i < strlen ; i++)
	{
		name[i] = file_name.at(i);
	}
	name[strlen] = '\0';
    // ��������� ���� ��� ������
    fstream file(name, ios::in);
    // ��� ������ ���������� �� �����
    if (file.fail()) return false;
    
    filter.clear(); //������� �������
    
    float filter_sample = 0.; // ������� ����������� �������
    
    while(!file.eof() && !file.fail()) //������ �� ����� �������������
    {
        file >> filter_sample;
        filter.push_back(filter_sample);
    }
    
    file.close(); //�������� �����

/*	unsigned size = filter.size();
	unsigned padded_size = size;

	if ((size & (size - 1)) != 0)
	{
		padded_size = 1;
		while (padded_size < size) padded_size <<= 1;
	}
	filter.resize(padded_size, complex <float> (0.0, 0.0));

	fft(filter, -1);

	for (int i = 0; i < filter.size()/2; i++) filter[i] *= 2.0f;
	for (int i = filter.size() / 2; i < filter.size(); i++) filter[i] = 0.0f;

	fft(filter, 1);*/

    return (filter.size() > 1); //���� � ������� ������������� 1 �������, �� ��������� ������
}

long factorial(int k)
{
	if (k == 0) return 1;
	return k * factorial(k - 1);
}

/** ���������������� ������� ������� ������� ���� �������� ������� */
double Bessel(double x)
{
	const double epsilon = 1E-8;

	double res = 1.0;
	int k = 1;
	double term = 1.0, prev_term = 1.0;

	// ����� ����, ��������� ������� �� ��������� ��������
	// ���� ��������� ��������� �� ����� ���������� �� ����������� �� ��������, �� ������� �������
	do
	{
		prev_term = term;
		term = (pow(x / 2.0, k) / factorial(k)) * (pow(x / 2.0, k) / factorial(k));
		res += term;
		k++;
	} while (fabs(term - prev_term) > epsilon);

	return res;
}

/** ������������� ��������� ���-������
* freq1				������ ������� ����� (������� ������� - ��, ���, ��� � �.�. �����, ����� ��� ������� ���� � ����� ��������)
* freq2				������� ������� �����
* trans_zone_width	������ ���������� ����
* sampling_rate		������� �������������
* fir					���������� �������������� ���-�������
*/
void __stdcall CreateFirFilter(	double freq1,
								double freq2,
								double trans_zone_width,
								double sampling_rate,
								vector <complex <float> > &fir)
{
	/** ������������� ������ ������� ����� */
	double rel_freq1 = freq1 / sampling_rate;
	/** ������������� ������� ������� ����� */
	double rel_freq2 = freq2 / sampling_rate;
	/** ������� ������� ������������� ������ �� ������������� ������ ���������� ���� */
	int order = (int)/*3.3 **/5* sampling_rate / trans_zone_width;
	if ((order % 2) == 0) order++;

	fir.clear();
	fir.resize(order, complex <float>(0.0, 0.0));
	double freq_width = rel_freq2 - rel_freq1;
	double center_freq = 0.5 * (rel_freq1 + rel_freq2);
	//fir[order / 2] = 2.0 * (rel_freq2 - rel_freq1);

	/** ������ ���������� �������������� ���������� ������� ��� �������� ���������� ������������� ���� ��� (�������� ������) */
	for (unsigned sample = 1; sample < order / 2; sample++)
	{
		fir[order / 2 + sample] = complex <float>((sin(/*2.0 **/ PI * /*rel_freq2*/freq_width * sample) +
			sin(/*2.0 **/ PI * /*rel_freq1*/freq_width * sample)) / (PI * sample),
			0.0);
	}

	for (unsigned sample = 1; sample < order / 2; sample++)
	{
		fir[sample] = fir[order - sample]/*(sin(2.0 * PI * rel_freq2 * (order - sample)) -
			sin(2.0 * PI * rel_freq1 * (order - sample))) / (PI * (order - sample))*/;
	}

	//unsigned n1 = (unsigned)((1.0 - freq_width / 2.0) * order);
	//unsigned n2 = (unsigned)(freq_width * order / 2.0);

	//if (n2 > n1) std::fill(fir.begin() + n1, fir.begin() + n2, complex <float>(1.0, 0.0));
	//else
	//{
	//	std::fill(fir.begin() + n1, fir.end(), complex <float>(1.0, 0.0));
	//	std::fill(fir.begin(), fir.begin() + n2, complex <float>(1.0, 0.0));
	//}
	

	

	vector <double> kaiser_window(order, 0.0);
	double beta_param = 5.0;
	for (unsigned sample = 0; sample < order / 2; sample++)
	{
		kaiser_window[sample + order / 2] = fabs(Bessel(beta_param * sqrt(1.0 - ((2.0 * sample) / (order - 1.0)) *
			((2.0 * sample) / (order - 1.0))))) /
			Bessel(beta_param);
	}
	for (unsigned sample = 1; sample < order / 2; sample++)
	{
		kaiser_window[sample] = kaiser_window[order - sample];
	}

	//// ����������� �� ����� �������
	std::transform(fir.begin(), fir.end(), kaiser_window.begin(), fir.begin(),
		[&](complex <float> a, double b)
	{
		return complex <float>(a.real() * b, a.imag() * b);
	});

	vector <complex <float> > temp = fir;
	for (unsigned sample = 0; sample < order; sample++)
	{
		fir[sample] = complex <float>(0.0, 0.0);

		for (unsigned freq = 0; freq < order; freq++)
		{
			fir[sample] += complex <float>(
				temp[freq].real() * cos(2.0 * PI * sample * freq / order) + 
				temp[freq].imag() * sin(2.0 * PI * sample * freq / order),
				temp[freq].imag() * cos(2.0 * PI * sample * freq / order) -
				temp[freq].real() * sin(2.0 * PI * sample * freq / order));
		}
	}

	int shift_num = center_freq * order;
	temp = fir;

	if (shift_num >= 0)
	{
		for (int sample = 0; sample < order; sample++)
		{
			if ((sample - shift_num) < 0)
				fir[sample] = temp[sample - shift_num + order];
			else
				fir[sample] = temp[sample - shift_num];
		}
	}
	else
	{
		for (int sample = 0; sample < order; sample++)
		{
			if ((sample - shift_num) >= order)
				fir[sample] = temp[sample - shift_num - order];
			else
				fir[sample] = temp[sample - shift_num];
		}
	}

	temp = fir;
	for (unsigned sample = 0; sample < order; sample++)
	{
		fir[sample] = complex <float>(0.0, 0.0);

		for (unsigned freq = 0; freq < order; freq++)
		{
			fir[sample] += complex <float>(
				temp[freq].real() * cos(2.0 * PI * sample * freq / order) -
				temp[freq].imag() * sin(2.0 * PI * sample * freq / order),
				temp[freq].imag() * cos(2.0 * PI * sample * freq / order) +
				temp[freq].real() * sin(2.0 * PI * sample * freq / order));
		}
	}

	/*double phase = 0.0;
	for (unsigned sample = 0; sample < order; sample++)
	{
		fir[sample] = complex <float>(	fir[sample].real() * cos(phase) - fir[sample].imag() * sin(phase),
										fir[sample].imag() * cos(phase) + fir[sample].real() * sin(phase));
		phase -= 2.0 * PI * center_freq * sample;
	}*/

	kaiser_window.clear();
	vector <double>().swap(kaiser_window);
	temp.clear();
	vector <complex <float> >().swap(temp);
}