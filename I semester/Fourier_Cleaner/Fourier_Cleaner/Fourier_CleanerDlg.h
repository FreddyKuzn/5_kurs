﻿#include "GDIClass.h"
#include <gl/GL.h>
#include <vector>
#include <math.h>
#include <complex>
// Fourier_CleanerDlg.h: файл заголовка
//

#pragma once
using namespace std;

// Диалоговое окно CFourierCleanerDlg
class CFourierCleanerDlg : public CDialogEx
{
// Создание
public:
	CFourierCleanerDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FOURIER_CLEANER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	//Характеристики купалов
	int koor_X1;
	int koor_Y1;
	int koor_X2;
	int koor_Y2;
	int koor_X3;
	int koor_Y3;
	int koor_X4;
	int koor_Y4;
	int koor_X5;
	int koor_Y5;
	double SigmaX1;
	double SigmaX2;
	double SigmaX3;
	double SigmaX4;
	double SigmaX5;
	double SigmaY1;
	double SigmaY2;
	double SigmaY3;
	double SigmaY4;
	double SigmaY5;
	double Ampl1;
	double Ampl2;
	double Ampl3;
	double Ampl4;
	double Ampl5;
	double n_noize; //величина аддитивного шума
	double Clear_limit; //Предел очистки спектра
	double Epsilon1; //Величина отклонения 
	double Epsilon2;
	double Gauss_Domes_e1;
	double Gauss_Domes_e2;
	double Gauss_Domes_e3;
	vector<vector<double>> buffer_Epsilon;

	int N; //размер сетки моделирования

	//Переменные моделирования
	vector<vector<double>> Gauss_Domes;
	vector<vector<double>> Gauss_Domes2n;
	vector<vector<double>> Spectrum_PIC; //модуль фурье образа
	vector < vector<complex<double>>> Spectrum_Complex;//фурье образ
	GDIClass GDIobj1; //GDICLASS
	GDIClass GDIobj2; //GDICLASS

	void add_noize(vector<vector<double>>& Data, double noize_size); 
	void linear_interpolation(vector<double> Old_Data, vector<double> &New_Data);
	void Bilinear_interpolation(vector<vector<double>> Old_Data, vector<vector<double>>& New_Data);
	void linear_interpolation(vector<double> Old_Data, vector<double>& New_Data,int N);
	void Bilinear_interpolation(vector<vector<double>> Old_Data, vector<vector<double>>& New_Data,int N);
	int step2(int sizein);//возвращает ближайшее значение pow(2,n)
	void fur(vector <complex<double>>& data, int is); //чистый фурье
	void fur_2d(vector<vector<complex<double>>> & data, int is); //двумерное Фурье
	void matr_trans(vector<vector<complex<double>>>& data);
	void matr_trans(vector<vector<double>>& data);
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedCheck1();
	bool View_Type;

	vector<vector<double>> Spectrum_PIC2; //модуль фурье образа

	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton4();
	double Zoom;

	//для картинок
	//указатель на окно картинки
	CWnd* hWnd;
	//указатель контекст устройство
	CDC* hdc;
	//хранит размер окна
	CRect size;
	CDC* mdc;
	CBitmap* my_buffer;
	CSliderCtrl Pic_slider;
	//Special for FROSTMAN
	void FrostDraw(vector<vector<double>>& data, GDIClass &ob);

};
