﻿
// Fourier_CleanerDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Fourier_Cleaner.h"
#include "Fourier_CleanerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define GAUSS(x,y,x0,y0,sx,sy,ampl) ampl*exp(-(       ((x-x0)*(x-x0))/(sx*sx)     +   ((y-y0)*(y-y0))/(sy*sy)   ))  
// Диалоговое окно CFourierCleanerDlg



CFourierCleanerDlg::CFourierCleanerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FOURIER_CLEANER_DIALOG, pParent)
	, koor_X1(50)
	, koor_Y1(50)
	, koor_X2(25)
	, koor_Y2(25)
	, koor_X3(25)
	, koor_Y3(75)
	, koor_X4(75)
	, koor_Y4(75)
	, koor_X5(75)
	, koor_Y5(25)
	, SigmaX1(10)
	, SigmaX2(10)
	, SigmaX3(10)
	, SigmaX4(10)
	, SigmaX5(10)
	, SigmaY1(10)
	, SigmaY2(10)
	, SigmaY3(10)
	, SigmaY4(10)
	, SigmaY5(10)
	, Ampl1(10)
	, Ampl2(10)
	, Ampl3(10)
	, Ampl4(10)
	, Ampl5(10)
	, n_noize(0)
	, Clear_limit(90)
	, Epsilon1(0)
	, N(100)
	, Zoom(2)
	, Epsilon2(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFourierCleanerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT2, koor_X1);
	DDX_Text(pDX, IDC_EDIT7, koor_Y1);
	DDX_Text(pDX, IDC_EDIT3, koor_X2);
	DDX_Text(pDX, IDC_EDIT8, koor_Y2);
	DDX_Text(pDX, IDC_EDIT4, koor_X3);
	DDX_Text(pDX, IDC_EDIT9, koor_Y3);
	DDX_Text(pDX, IDC_EDIT5, koor_X4);
	DDX_Text(pDX, IDC_EDIT10, koor_Y4);
	DDX_Text(pDX, IDC_EDIT6, koor_X5);
	DDX_Text(pDX, IDC_EDIT11, koor_Y5);
	DDX_Text(pDX, IDC_EDIT12, SigmaX1);
	DDX_Text(pDX, IDC_EDIT13, SigmaX2);
	DDX_Text(pDX, IDC_EDIT14, SigmaX3);
	DDX_Text(pDX, IDC_EDIT15, SigmaX4);
	DDX_Text(pDX, IDC_EDIT16, SigmaX5);
	DDX_Text(pDX, IDC_EDIT17, SigmaY1);
	DDX_Text(pDX, IDC_EDIT18, SigmaY2);
	DDX_Text(pDX, IDC_EDIT19, SigmaY3);
	DDX_Text(pDX, IDC_EDIT20, SigmaY4);
	DDX_Text(pDX, IDC_EDIT21, SigmaY5);
	DDX_Text(pDX, IDC_EDIT22, Ampl1);
	DDX_Text(pDX, IDC_EDIT23, Ampl2);
	DDX_Text(pDX, IDC_EDIT24, Ampl3);
	DDX_Text(pDX, IDC_EDIT25, Ampl4);
	DDX_Text(pDX, IDC_EDIT26, Ampl5);
	DDX_Text(pDX, IDC_EDIT27, n_noize);
	DDX_Text(pDX, IDC_EDIT28, Clear_limit);
	DDX_Text(pDX, IDC_EDIT29, Epsilon1);
	DDX_Text(pDX, IDC_EDIT30, N);
	DDX_Control(pDX, IDC_PIC1, GDIobj1); //ОТРИСОВКА GDI+
	DDX_Control(pDX, IDC_PIC2, GDIobj2); //ОТРИСОВКА GDI+
	DDX_Text(pDX, IDC_EDIT1, Zoom);
	DDX_Control(pDX, IDC_SLIDER1, Pic_slider);
	DDX_Text(pDX, IDC_EDIT31, Epsilon2);
}

BEGIN_MESSAGE_MAP(CFourierCleanerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CFourierCleanerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CFourierCleanerDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CFourierCleanerDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_CHECK1, &CFourierCleanerDlg::OnBnClickedCheck1)
	ON_BN_CLICKED(IDC_BUTTON2, &CFourierCleanerDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &CFourierCleanerDlg::OnBnClickedButton4)
END_MESSAGE_MAP()


// Обработчики сообщений CFourierCleanerDlg

BOOL CFourierCleanerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	GDIobj1.Invalidate();
	View_Type = false;
	Pic_slider.SetRangeMin(1);
	Pic_slider.SetRangeMax(3);
	Pic_slider.SetPos(1);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CFourierCleanerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CFourierCleanerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFourierCleanerDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}

void CFourierCleanerDlg::OnBnClickedCheck1()
{
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	if (View_Type)
	{
		if (Gauss_Domes.size() != 0)
		{
			/*GDIobj1.GradientCell(Gauss_Domes);
			GDIobj1.MyPaint();*/
			FrostDraw(Gauss_Domes, GDIobj1);
			View_Type = false;
		}
	}
	else
	{
		if (Spectrum_PIC.size() != 0)
		{
			/*GDIobj1.GradientCell(Spectrum_PIC);
			GDIobj1.MyPaint();*/
			FrostDraw(Spectrum_PIC, GDIobj1);
			View_Type = true;
		}
	}
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
}
void CFourierCleanerDlg::OnBnClickedButton1() //Default
{ //Начальная установка
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	Spectrum_PIC.clear();
	Gauss_Domes.clear();

	switch (Pic_slider.GetPos())
	{
	case 1:
	{
		Gauss_Domes.resize(N);
		for (int i = 0; i < N; i++)
		{
			Gauss_Domes[i].resize(N);
			for (int j = 0; j < N; j++)
			{
				double buf = SigmaY1;
				Gauss_Domes[i][j] = 0;
				Gauss_Domes[i][j] += GAUSS(i, j, koor_X1, koor_Y1, SigmaX1, SigmaY1, Ampl1) +
					GAUSS(i, j, koor_X2, koor_Y2, SigmaX2, SigmaY2, Ampl2) +
					GAUSS(i, j, koor_X3, koor_Y3, SigmaX3, SigmaY3, Ampl3) +
					GAUSS(i, j, koor_X4, koor_Y4, SigmaX4, SigmaY4, Ampl4) +
					GAUSS(i, j, koor_X5, koor_Y5, SigmaX5, SigmaY5, Ampl5);
			}
		}
		break;
	}
	case 2:
	{
		vector<vector<double>> image;
		CImage img;
		img.Load(CString("Pic1.bmp"));

		hWnd = GetDlgItem(IDC_PIC1);
		hdc = hWnd->GetDC();
		hWnd->GetClientRect(&size);
		CDC* mdc = new CDC();
		mdc->CreateCompatibleDC(hdc);

		CBitmap bmp_line;
		bmp_line.CreateCompatibleBitmap(hdc, size.Width(), size.Height());
		//сделали объект текущим
		CBitmap* tmp_line = mdc->SelectObject(&bmp_line);
		COLORREF color;
		int p = 425;
		Gauss_Domes.resize(p);
		for (int i = 0; i < p; i++)
		{
			Gauss_Domes[i].resize(p);
		}
		for (int i = 0; i < p; i++)
		{
			for (int j = 0; j < p; j++)
			{
				color = img.GetPixel(i, j);
				Gauss_Domes[i][j] = (GetRValue(color) + GetGValue(color) + GetBValue(color)) / 3;
			}
		}
	}
	break;
	case 3:
	{
		vector<vector<double>> image;
		CImage img;
		img.Load(CString("Pic2.bmp"));

		hWnd = GetDlgItem(IDC_PIC1);
		hdc = hWnd->GetDC();
		hWnd->GetClientRect(&size);
		CDC* mdc = new CDC();
		mdc->CreateCompatibleDC(hdc);

		CBitmap bmp_line;
		bmp_line.CreateCompatibleBitmap(hdc, size.Width(), size.Height());
		//сделали объект текущим
		CBitmap* tmp_line = mdc->SelectObject(&bmp_line);
		COLORREF color;
		int p = 455;
		Gauss_Domes.resize(p);
		for (int i = 0; i < p; i++)
		{
			Gauss_Domes[i].resize(p);
		}
		for (int i = 0; i < p; i++)
		{
			for (int j = 0; j < p; j++)
			{
				color = img.GetPixel(i, j);
				Gauss_Domes[i][j] = (GetRValue(color) + GetGValue(color) + GetBValue(color)) / 3;
			}
		}
	}
	break;
	}



	//////////////////////////////////
	Gauss_Domes2n.clear();
	Bilinear_interpolation(Gauss_Domes, Gauss_Domes2n);
	Gauss_Domes.clear();
	Gauss_Domes = Gauss_Domes2n;
	Gauss_Domes2n.clear();
	//////////////////////////////////////////////////////////////
	Gauss_Domes_e1 = 0;
	Gauss_Domes_e2 = 0;
	for (int i = 0; i < Gauss_Domes.size(); i++)
	{
		for (int j = 0; j < Gauss_Domes[i].size(); j++)
		{
			Gauss_Domes_e1 += sqrt(Gauss_Domes[i][j] * Gauss_Domes[i][j]);
		}
	}
	buffer_Epsilon.clear();
	buffer_Epsilon = Gauss_Domes;
	add_noize(Gauss_Domes, n_noize);
	for (int i = 0; i < Gauss_Domes.size(); i++)
	{
		for (int j = 0; j < Gauss_Domes[i].size(); j++)
		{
			Gauss_Domes_e2 += sqrt(fabs(Gauss_Domes[i][j] * Gauss_Domes[i][j] - buffer_Epsilon[i][j] * buffer_Epsilon[i][j]));
		}
	}
	Epsilon1 = Gauss_Domes_e2 / Gauss_Domes_e1;
	//////////////////////////////////////////////////////////////
	//Заполняем вектор Spectrum_Complex комплексными значениями
	Spectrum_Complex.clear();
	Spectrum_Complex.resize(Gauss_Domes.size());
	for (int i = 0; i < Spectrum_Complex.size(); i++)
	{
		for (int j = 0; j < Gauss_Domes[i].size(); j++)
		{
			complex<double> buf = Gauss_Domes[i][j];
			Spectrum_Complex[i].push_back(buf);
		}
	}
	//Фурье
	fur_2d(Spectrum_Complex, -1);
	//Получение модуля
	Spectrum_PIC.clear();
	Spectrum_PIC.resize(Spectrum_Complex.size());
	for (int i = 0; i < Spectrum_Complex.size(); i++)
	{
		for (int j = 0; j < Spectrum_Complex[i].size(); j++)
		{
			double buf = sqrt(pow(Spectrum_Complex[i][j].real(), 2) + pow(Spectrum_Complex[i][j].imag(), 2));
			buf = log(1. + buf);
			Spectrum_PIC[i].push_back(buf);
		}
	}
	Spectrum_PIC[0][0] = 0; //для лучшего отображения
	/////// Меняем квадранты местами
	int half_size = (int)(Spectrum_PIC.size() / 2);
	for (int i = 0; i < half_size; i++)
	{
		for (int j = 0; j < half_size; j++)
		{
			//Меняем II и IV
			double buffer_kvadrant = Spectrum_PIC[i][j];
			Spectrum_PIC[i][j] = Spectrum_PIC[i + half_size][j + half_size];
			Spectrum_PIC[i + half_size][j + half_size] = buffer_kvadrant;
			//Меняем I и III
			buffer_kvadrant = Spectrum_PIC[i + half_size][j];
			Spectrum_PIC[i + half_size][j] = Spectrum_PIC[i][j + half_size];
			Spectrum_PIC[i][j + half_size] = buffer_kvadrant;
		}
	}
	///////
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	/*GDIobj1.GradientCell(Gauss_Domes);
	GDIobj1.MyPaint();*/
	FrostDraw(Gauss_Domes, GDIobj1);
	UpdateData(0);
}
void CFourierCleanerDlg::OnBnClickedButton3() //FCleaner
{
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	int half_size = (int)(Spectrum_Complex.size() / 2);
	double FullEnergy = 0;
	for (int i = 0; i < Spectrum_Complex.size(); i++)
	{
		for (int j = 0; j < Spectrum_Complex[i].size(); j++)
		{
			FullEnergy += sqrt(pow(Spectrum_Complex[i][j].real(), 2) + pow(Spectrum_Complex[i][j].imag(), 2));
		}
	}
	FullEnergy -= sqrt(pow(Spectrum_Complex[0][0].real(), 2) + pow(Spectrum_Complex[0][0].imag(), 2));
	double buffer_Energy = 0;
	int drop;
	for (drop = 0; drop < half_size; drop++)
	{
		buffer_Energy = 0;
		for (int i = 0; i < drop; i++)
		{
			for (int j = 0; j < drop; j++)
			{
				buffer_Energy += sqrt(pow(Spectrum_Complex[i][j].real(), 2) + pow(Spectrum_Complex[i][j].imag(), 2));
				buffer_Energy += sqrt(pow(Spectrum_Complex[Spectrum_Complex.size() - i - 1][j].real(), 2) + pow(Spectrum_Complex[Spectrum_Complex.size() - i - 1][j].imag(), 2));
				buffer_Energy += sqrt(pow(Spectrum_Complex[Spectrum_Complex.size() - i - 1][Spectrum_Complex.size() - j - 1].real(), 2) + pow(Spectrum_Complex[Spectrum_Complex.size() - 1 - i][Spectrum_Complex.size() - 1 - j].imag(), 2));
				buffer_Energy += sqrt(pow(Spectrum_Complex[i][Spectrum_Complex.size() - 1 - j].real(), 2) + pow(Spectrum_Complex[i][Spectrum_Complex.size() - 1 - j].imag(), 2));
			}
		}
		buffer_Energy -= sqrt(pow(Spectrum_Complex[0][0].real(), 2) + pow(Spectrum_Complex[0][0].imag(), 2));
		if (buffer_Energy >= (Clear_limit * FullEnergy / 100)) break;
	}
	for (int i = drop; i < Spectrum_Complex.size() - drop; i++)
	{
		for (int j = 0; j < Spectrum_Complex.size(); j++)
		{
			Spectrum_Complex[i][j] = 0;
		}
	}
	for (int i = 0; i < Spectrum_Complex.size(); i++)
	{
		for (int j = drop; j < Spectrum_Complex.size() - drop; j++)
		{
			Spectrum_Complex[i][j] = 0;
		}
	}
	//Получение модуля
	Spectrum_PIC.clear();
	Spectrum_PIC.resize(Spectrum_Complex.size());
	for (int i = 0; i < Spectrum_Complex.size(); i++)
	{
		for (int j = 0; j < Spectrum_Complex[i].size(); j++)
		{
			double buf = sqrt(pow(Spectrum_Complex[i][j].real(), 2) + pow(Spectrum_Complex[i][j].imag(), 2));
			buf = log(1. + buf);
			Spectrum_PIC[i].push_back(buf);
		}
	}
	Spectrum_PIC[0][0] = 0; //для лучшего отображения
	/////// Меняем квадранты местами
	for (int i = 0; i < half_size; i++)
	{
		for (int j = 0; j < half_size; j++)
		{
			//Меняем II и IV
			double buffer_kvadrant = Spectrum_PIC[i][j];
			Spectrum_PIC[i][j] = Spectrum_PIC[i + half_size][j + half_size];
			Spectrum_PIC[i + half_size][j + half_size] = buffer_kvadrant;
			//Меняем I и III
			buffer_kvadrant = Spectrum_PIC[i + half_size][j];
			Spectrum_PIC[i + half_size][j] = Spectrum_PIC[i][j + half_size];
			Spectrum_PIC[i][j + half_size] = buffer_kvadrant;
		}
	}
	///////
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
	/*GDIobj1.GradientCell(Spectrum_PIC);
	GDIobj1.MyPaint();*/
	FrostDraw(Spectrum_PIC, GDIobj1);
	UpdateData(0);
}

void CFourierCleanerDlg::OnBnClickedButton2() //new
{
	UpdateData(1);
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	Spectrum_PIC2.clear();
	Spectrum_PIC2.resize(Spectrum_Complex.size());
	vector < vector<complex<double>>> Spectrum_Complex_buf = Spectrum_Complex;
	fur_2d(Spectrum_Complex_buf, 1);
	Gauss_Domes_e3 = 0;
	for (int i = 0; i < Spectrum_PIC2.size(); i++)
	{
		Spectrum_PIC2[i].resize(Spectrum_Complex_buf[i].size());
		for (int j = 0; j < Spectrum_PIC2[i].size(); j++)
		{
			Spectrum_PIC2[i][j] = Spectrum_Complex_buf[i][j].real();
			Gauss_Domes_e3 += sqrt(fabs(Spectrum_Complex_buf[i][j].real() * Spectrum_Complex_buf[i][j].real() - buffer_Epsilon[i][j] * buffer_Epsilon[i][j]));
		}
	}
	Epsilon2 = Gauss_Domes_e3 / Gauss_Domes_e1;
	/*GDIobj2.GradientCell(Spectrum_PIC2);
	GDIobj2.MyPaint();*/
	FrostDraw(Spectrum_PIC2, GDIobj2);
	UpdateData(0);
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
}

void CFourierCleanerDlg::add_noize(vector<vector<double>>& Data, double noize_size) //new
{
	vector<vector<double>> noize;
	noize.resize(Data.size());
	for (int i = 0; i < Data.size(); i++)
	{
		noize[i].resize(Data[i].size());
		for (int j = 0; j < noize[i].size(); j++)
		{
			noize[i][j] = 0;
		}
	}
	double alfa;
	double sum_signal = 0;
	double sum_shum = 0;
	double Eh;
	for (int i = 0; i < Data.size(); i++)
	{
		for (int j = 0; j < Data[i].size(); j++)
		{
			double M, ksi;
			M = rand() % 9 + 12;
			ksi = 0;
			for (int k = 0; k < M; k++)
			{
				ksi += -1 + 2 * rand() * 1. / RAND_MAX;
			}
			noize[i][j] = ksi / M;

			sum_signal += Data[i][j] * Data[i][j];
			sum_shum += noize[i][j] * noize[i][j];
		}
	}
	Eh = noize_size * sum_signal / 100;
	alfa = sqrt(Eh / sum_shum);
	for (int i = 0; i < Data.size(); i++)
	{
		for (int j = 0; j < Data[i].size(); j++)
		{
			Data[i][j] += alfa * noize[i][j];
		}
	}
}

void CFourierCleanerDlg::linear_interpolation(vector<double> Old_Data, vector<double>& New_Data)
{
	/*New_Data.clear();
	int new_size = step2(Old_Data.size());
	New_Data.push_back(Old_Data[0]);
	double step = (double)(Old_Data.size() - 1) / ((double)new_size - 1);
	for (int j = 1; j < new_size; j++)
	{
		double pos = step * j;
		int next;
		next = (int)ceil(pos);
		if (next!= Old_Data.size())
		New_Data.push_back(((double)next - pos) * Old_Data[next - 1] + (pos - next + 1) * Old_Data[next]);
		else
		{
			next--;
			New_Data.push_back(((double)next - pos) * Old_Data[next - 1] + (pos - next + 1) * Old_Data[next]);
		}
	}*/


	struct abc
	{
		double a, b, c;
	};
	vector<abc> koeff;
	koeff.resize(Old_Data.size() - 2);
	for (int i = 0; i < koeff.size(); i++)
	{
		koeff[i].c = (Old_Data[i + 2] - Old_Data[i]) / 2;
		koeff[i].c -= (Old_Data[i + 1] - Old_Data[i]);
		koeff[i].b = (Old_Data[i + 1] - Old_Data[i]) - koeff[i].c * (i+1+i);
		koeff[i].a = Old_Data[i] - koeff[i].b * i - koeff[i].c * i * i;
	}
	int new_size = step2(Old_Data.size());
	New_Data.clear();
	New_Data.push_back(Old_Data[0]);
	double step = (double)(Old_Data.size() - 1) / ((double)new_size - 1); //новый шаг
	for (int j = 1; j < new_size; j++)
	{
		double pos = step * j; // коорд новой точки
		int next;
		int ii = (int)floor(pos); // ближайший левый сосед
		if (ii >= koeff.size()) ii = koeff.size()-1;
		double buf = koeff[ii].a + koeff[ii].b * pos + koeff[ii].c*pos*pos;
		New_Data.push_back(buf);
	}
}
void CFourierCleanerDlg::Bilinear_interpolation(vector<vector<double>> Old_Data, vector<vector<double>>& New_Data)
{
	New_Data.clear();
	for (int i = 0; i < Old_Data.size(); i++)
	{
		vector<double> buffer;
		linear_interpolation(Old_Data[i], buffer);
		New_Data.push_back(buffer);
	}
	Old_Data.clear();
	Old_Data = New_Data;
	New_Data.clear();
	matr_trans(Old_Data);
	for (int i = 0; i < Old_Data.size(); i++)
	{
		vector<double> buffer;
		linear_interpolation(Old_Data[i], buffer);
		New_Data.push_back(buffer);
	}
	matr_trans(New_Data);//new
}
void CFourierCleanerDlg::linear_interpolation(vector<double> Old_Data, vector<double>& New_Data, int N)
{
	New_Data.clear();
	int new_size = N;
	New_Data.push_back(Old_Data[0]);
	double step = (double)(Old_Data.size() - 1) / ((double)new_size - 1);
	for (int j = 1; j < new_size; j++)
	{
		double pos = step * j;
		int next;
		next = (int)ceil(pos);
		if (next != Old_Data.size())
			New_Data.push_back(((double)next - pos) * Old_Data[next - 1] + (pos - next + 1) * Old_Data[next]);
		else
		{
			next--;
			New_Data.push_back(((double)next - pos) * Old_Data[next - 1] + (pos - next + 1) * Old_Data[next]);
		}
	}
}
void CFourierCleanerDlg::Bilinear_interpolation(vector<vector<double>> Old_Data, vector<vector<double>>& New_Data, int N)
{
	New_Data.clear();
	for (int i = 0; i < Old_Data.size(); i++)
	{
		vector<double> buffer;
		linear_interpolation(Old_Data[i], buffer, N);
		New_Data.push_back(buffer);
	}
	Old_Data.clear();
	Old_Data = New_Data;
	New_Data.clear();
	matr_trans(Old_Data);
	for (int i = 0; i < Old_Data.size(); i++)
	{
		vector<double> buffer;
		linear_interpolation(Old_Data[i], buffer, N);
		New_Data.push_back(buffer);
	}
	matr_trans(New_Data);//new
}

int CFourierCleanerDlg::step2(int sizein)
{
	int i = 0;
	double S = sizein;
	for (;;)
	{
		if (S > 1)
		{
			i++;
			S /= 2;
		}
		else break;
	}
	return pow(2, i);
}
void CFourierCleanerDlg::fur(vector <complex <double>>& data, int is)
{
	int i, j, istep, n;
	n = data.size();
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926f;

	r = pi * is;
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real();
			temp_i = data[j].imag();
			data[j] = data[i];
			data[i] = temp_r + complex <double>(0, 1) * temp_i;

		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r * data[j].real() - w_i * data[j].imag();
				temp_i = w_r * data[j].imag() + w_i * data[j].real();
				data[j] = (data[i].real() - temp_r) + complex <double>(0, 1) * (data[i].imag() - temp_i);
				data[i] += (temp_r)+complex <double>(0, 1) * (temp_i);
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i] /= (double)n;
		}
}
void CFourierCleanerDlg::fur_2d(vector<vector<complex<double>>>& data, int is)
{
	for (int i = 0; i < data.size(); i++)
	{
		fur(data[i], is);
	}
	matr_trans(data);
	for (int i = 0; i < data.size(); i++)
	{
		fur(data[i], is);
	}
	matr_trans(data);//new
}

void CFourierCleanerDlg::matr_trans(vector<vector<complex<double>>>& data)
{
	vector<vector<complex<double>>> buffer;
	buffer.resize(data[0].size());
	for (int i = 0; i < buffer.size(); i++)
		buffer[i].resize(data.size());
	for (int i = 0; i < data.size(); i++)
	{
		for (int j = 0; j < data[i].size(); j++)
		{
			buffer[j][i] = data[i][j];
		}
	}
	data.clear();
	data = buffer;
	buffer.clear();
}
void CFourierCleanerDlg::matr_trans(vector<vector<double>>& data)
{
	vector<vector<double>> buffer;
	buffer.resize(data[0].size());
	for (int i = 0; i < buffer.size(); i++)
		buffer[i].resize(data.size());
	for (int i = 0; i < data.size(); i++)
	{
		for (int j = 0; j < data[i].size(); j++)
		{
			buffer[j][i] = data[i][j];
		}
	}
	data.clear();
	data = buffer;
	buffer.clear();
}



void CFourierCleanerDlg::OnBnClickedButton4()
{
	UpdateData(1);
	koor_X1 *= Zoom;
	koor_Y1 *= Zoom;
	koor_X2 *= Zoom;
	koor_Y2 *= Zoom;
	koor_X3 *= Zoom;
	koor_Y3 *= Zoom;
	koor_X4 *= Zoom;
	koor_Y4 *= Zoom;
	koor_X5 *= Zoom;
	koor_Y5 *= Zoom;
	SigmaX1 *= Zoom;
	SigmaX2 *= Zoom;
	SigmaX3 *= Zoom;
	SigmaX4 *= Zoom;
	SigmaX5 *= Zoom;
	SigmaY1 *= Zoom;
	SigmaY2 *= Zoom;
	SigmaY3 *= Zoom;
	SigmaY4 *= Zoom;
	SigmaY5 *= Zoom;
	N *= Zoom;
	UpdateData(0);
}
void CFourierCleanerDlg::FrostDraw(vector<vector<double>>& data, GDIClass& ob)
{
	vector<vector<double>> PicData;
	if (data.size() < 512)	Bilinear_interpolation(data, PicData, 512);
	else PicData = data;
	ob.GradientCell(PicData);
	ob.MyPaint();
}