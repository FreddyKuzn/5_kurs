﻿
// Fourier_Cleaner.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CFourierCleanerApp:
// Сведения о реализации этого класса: Fourier_Cleaner.cpp
//

class CFourierCleanerApp : public CWinApp
{
public:
	CFourierCleanerApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CFourierCleanerApp theApp;
