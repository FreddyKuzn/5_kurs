﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется FourierCleaner.rc
//
#define IDD_FOURIER_CLEANER_DIALOG      102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_EDIT2                       1003
#define IDC_EDIT3                       1004
#define IDC_EDIT4                       1005
#define IDC_EDIT5                       1006
#define IDC_EDIT6                       1007
#define IDC_EDIT7                       1008
#define IDC_EDIT8                       1009
#define IDC_EDIT9                       1010
#define IDC_EDIT10                      1011
#define IDC_EDIT11                      1012
#define IDC_PIC1                        1013
#define IDC_PIC2                        1014
#define IDC_VIEWER                      1017
#define IDC_EDIT12                      1018
#define IDC_EDIT13                      1019
#define IDC_EDIT14                      1020
#define IDC_EDIT15                      1021
#define IDC_EDIT16                      1022
#define IDC_EDIT17                      1023
#define IDC_EDIT18                      1024
#define IDC_EDIT19                      1025
#define IDC_EDIT20                      1026
#define IDC_EDIT21                      1027
#define IDC_EDIT22                      1028
#define IDC_EDIT23                      1029
#define IDC_EDIT24                      1030
#define IDC_EDIT25                      1031
#define IDC_EDIT26                      1032
#define IDC_EDIT27                      1033
#define IDC_EDIT28                      1034
#define IDC_EDIT29                      1035
#define IDC_EDIT30                      1036
#define IDC_BUTTON3                     1037
#define IDC_CHECK1                      1038
#define IDC_BUTTON2                     1039
#define IDC_BUTTON4                     1040
#define IDC_EDIT1                       1041
#define IDC_EDIT31                      1042
#define IDC_SLIDER1                     1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
