﻿
// Experiment_oneDlg.h: файл заголовка
//
#include "GDIClass.h"
#include <vector>
#include <math.h>
#include "ChartViewer.h"
using namespace std;

// Диалоговое окно CExperimentoneDlg
class CExperimentoneDlg : public CDialogEx
{
	// Создание
public:
	CExperimentoneDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_EXPERIMENT_ONE_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	struct Dot
	{
		double x = 0;
		double y = 0;
		bool Border = false;
		double v_x = 0;
		double v_y = 0;
		double F_x = 0;
		double F_y = 0;
	};
	//+ конастанты
	double const D = 1.648e-21; //Дж
	double const r_0 = 0.382 * pow(10, -9);
	double tau;
	double dtau; //шаг по времени
	double m_argon = 39.948 * 1.6605390666 * pow(10, -27);
	//double m_argon = 0.0539267 * pow(10, -6); //масса атома
	int N;
	int time; //кол-во итераций которые прошли
	int dots_count;
	int dots_count_real;
	bool timerOnOff; //true - таймер включён, false - выключен
	bool radio; //тип заполнения
	vector<Dot> dots;

	GDIClass gdiObj;

	CButton radio_control1;
	CButton radio_control2;
	CButton Check_vakansii;
	CButton bt_START_STOP;

	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	double F_ik(vector<Dot>dots, int i, int k, int j);//i,k - индексы атомов. j - номер проекции (0-x; 1-y); расчёт силы,\
	действующей со стороны атома k на атом i
	double koeff_K(Dot d1, Dot d2); //Расчёт обрезающего коэффициента
	void iteration();//функция расчёта новый координат и скоростей на новой итерации
	void gdiPaintDots(); //отрисовка вектора dots в gdi
	vector <double>E_pot;
	vector <double>E_kin;
	vector <double> E_full;
	void E_kin_func(); // функция расчета кин энергии
	void E_pot_func(); // функция расчета пот энергии
	void E_full_func(); // функция расчета полн энергии
	CChartViewer viewer;
	DoubleArray vectorToArray(vector<double>& v);
	void ViewerDraw();
	/////////////////////////////////////// Переменные для переопределения скоростей
	double Temp; // температура системы в Кельвинах
	double mV_2usr; //переменная для усреднения по 20 шагам
	double k_b = 1.38064852 * pow(10, -23);//Больцмана
	double koeff_B(double& buf); // коэф. перерасчёта скоростей
	BOOL tempOn;
	/////////////////////////////////////// парная корреляционная функция
	vector <double> func_korr;
	CChartViewer viewer_korr; //визуализация
	void viewer_korr_Draw();
	void par_korr_func(vector<double> &func, int kk);
	///
	int GetBliz(double x); //получение ближайшей целой координаты
	vector<double> func_korr_abs;
	double temp_real;
	BOOL bord;
	BOOL tmr_kill;
	/////////////////
	int time_en;
	double en_buffer;
	double en_buffer_local;
};
