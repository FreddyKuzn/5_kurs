﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Experimentone.rc
//
#define IDD_EXPERIMENT_ONE_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define IDC_EDIT3                       1002
#define IDC_BUTTON1                     1004
#define IDC_RADIO1                      1005
#define IDC_RADIO2                      1006
#define IDC_PIC1                        1007
#define IDC_CHECK1                      1013
#define IDC_BUTTON2                     1014
#define IDC_CHECK2                      1015
#define IDC_EDIT4                       1016
#define IDC_FULL_ENERGY                 1017
#define IDC_FUNC                        1018
#define IDC_EDIT5                       1019
#define IDC_EDIT6                       1020
#define IDC_EDIT7                       1021
#define IDC_CHECK3                      1022
#define IDC_CHECK4                      1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
