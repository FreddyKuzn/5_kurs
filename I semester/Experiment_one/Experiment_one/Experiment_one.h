﻿
// Experiment_one.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CExperimentoneApp:
// Сведения о реализации этого класса: Experiment_one.cpp
//

class CExperimentoneApp : public CWinApp
{
public:
	CExperimentoneApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CExperimentoneApp theApp;
