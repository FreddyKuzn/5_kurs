#pragma once
#include <afxwin.h>
#include <math.h>
#include <vector>

using namespace std;
#define M_PI 3.1415926535
class GDIClass :
	public CStatic
{
	//////////////////////////////////////////
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
protected:
	DECLARE_MESSAGE_MAP()
public:
	int xmax = 100;
	int xmin = 0;
	int ymax = 100;
	int ymin = 0;
	INT32 zoom = 1000;
	struct GDIDot //�����
	{
		INT32 x0;
		INT32 y0;
	};
	std::vector <GDIDot> massDots;
	void MyPaint(int N)
	{
		N += 2;
		xmax = N * zoom;
		ymax = N * zoom;
		Invalidate();
	}
	void DotsToRound(vector<double> Data_x, vector<double> Data_y)
	{
		massDots.clear();
		for (int i = 0; i < Data_x.size(); i++)
		{
			Data_x[i] /= 0.382 * pow(10, -9);
			Data_y[i] /= 0.382 * pow(10, -9);
			GDIDot buf;
			buf.x0 = (Data_x[i] * zoom) + 0.75*zoom;
			buf.y0 = (Data_y[i] * zoom) + 0.75*zoom;
			massDots.push_back(buf);
		}
	}
};

