#include "pch.h"
#include "GDIClass.h"
#include "Experiment_one.h"
#include "Experiment_oneDlg.h"
using namespace Gdiplus;
IMPLEMENT_DYNAMIC(GDIClass, CStatic)
ULONG_PTR gdiPlusToken;
GDIClass::GDIClass()
{
	GdiplusStartupInput gdiPlusStartapInput;
	if (GdiplusStartup(&gdiPlusToken, &gdiPlusStartapInput, NULL) != Ok)
	{
		MessageBox(L"Init error", L"Error Message", MB_OK);
		exit(0);
	}
}
GDIClass::~GDIClass()
{
	GdiplusShutdown(gdiPlusToken);
}

BEGIN_MESSAGE_MAP(GDIClass, CStatic)
END_MESSAGE_MAP()

 void GDIClass::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	 Graphics g(lpDrawItemStruct->hDC);
	 Bitmap Map(lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		 lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1,
		 &g);
	 Graphics g2(&Map);
	 g2.SetSmoothingMode(SmoothingModeHighSpeed);
	 Color A((BYTE)255, (BYTE)255, (BYTE)255);
	 g2.Clear(A);
	 Matrix matr;
	 g2.ResetTransform();

	 Pen RED(Color::Red, 1);
	 Pen BLUE(Color::Blue, 1);
	 SolidBrush kist(Color::White);
	 Gdiplus::REAL kx = (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) / (double)(xmax - xmin); //���������� ������������
	 Gdiplus::REAL ky = (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) / (double)(ymax - ymin);
	 matr.Scale(kx, ky);
	 g2.SetTransform(&matr);
	 for (int i = 0; i < massDots.size(); i++)
	 {
		 g2.DrawEllipse(&RED, massDots[i].x0- zoom/4, massDots[i].y0- zoom/4, zoom, zoom);
	 }
	 g.DrawImage(&Map, 0, 0);
}