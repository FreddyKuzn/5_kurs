﻿
// Experiment_oneDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "Experiment_one.h"
#include "Experiment_oneDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define ATOM(x) (x*r_0)

// Диалоговое окно CExperimentoneDlg



CExperimentoneDlg::CExperimentoneDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EXPERIMENT_ONE_DIALOG, pParent)
	, N(20)
	, dots_count(250)
	, dots_count_real(0)
	, time(0)
	, Temp(300)
	, tempOn(FALSE)
	, tau(0.005)
	, temp_real(0)
	, bord(TRUE)
	, tmr_kill(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CExperimentoneDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, N);
	DDX_Text(pDX, IDC_EDIT2, dots_count);
	DDX_Control(pDX, IDC_RADIO1, radio_control1);
	DDX_Control(pDX, IDC_RADIO2, radio_control2);
	DDX_Text(pDX, IDC_EDIT3, dots_count_real);
	DDX_Control(pDX, IDC_PIC1, gdiObj); //ОТРИСОВКА GDI+
	DDX_Control(pDX, IDC_CHECK1, Check_vakansii);
	DDX_Control(pDX, IDC_BUTTON2, bt_START_STOP);
	DDX_Text(pDX, IDC_EDIT4, time);
	DDX_Control(pDX, IDC_FULL_ENERGY, viewer);
	DDX_Text(pDX, IDC_EDIT5, Temp);
	DDX_Check(pDX, IDC_CHECK2, tempOn);
	DDX_Control(pDX, IDC_FUNC, viewer_korr);
	DDX_Text(pDX, IDC_EDIT6, tau);
	DDX_Text(pDX, IDC_EDIT7, temp_real);
	DDX_Check(pDX, IDC_CHECK3, bord);
	DDX_Check(pDX, IDC_CHECK4, tmr_kill);
}

BEGIN_MESSAGE_MAP(CExperimentoneDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDCANCEL, &CExperimentoneDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CExperimentoneDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_RADIO1, &CExperimentoneDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CExperimentoneDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_BUTTON2, &CExperimentoneDlg::OnBnClickedButton2)
END_MESSAGE_MAP()


// Обработчики сообщений CExperimentoneDlg

BOOL CExperimentoneDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	radio_control1.SetCheck(1);
	radio = true;
	Check_vakansii.SetCheck(1);
	bt_START_STOP.CButton::EnableWindow(FALSE);
	bt_START_STOP.SetWindowText((LPCWSTR)L"Пуск");
	timerOnOff = false;
	ViewerDraw();
	viewer_korr_Draw();
	dtau = tau * 2 * pow(10, -12);
	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CExperimentoneDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CExperimentoneDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CExperimentoneDlg::OnBnClickedCancel()
{
	CDialogEx::OnCancel();
}


void CExperimentoneDlg::OnBnClickedButton1()//Initialization
{
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	UpdateData(1);
	dots.clear();
	////////////////////////////
	int buf_x = 0, buf_y = 0;//1
	for (buf_x = 0; buf_x <= N; buf_x++)
	{
		Dot buf_dot;
		buf_dot.Border = bord;
		buf_dot.x = buf_x;
		buf_dot.y = buf_y;
		dots.push_back(buf_dot);
	}
	buf_x = 0, buf_y = N;//2
	for (buf_x = 0; buf_x <= N; buf_x++)
	{
		Dot buf_dot;
		buf_dot.Border = bord;
		buf_dot.x = buf_x;
		buf_dot.y = buf_y;
		dots.push_back(buf_dot);
	}
	buf_x = 0, buf_y = 0;//3
	for (buf_y = 1; buf_y < N; buf_y++)
	{
		Dot buf_dot;
		buf_dot.Border = bord;
		buf_dot.x = buf_x;
		buf_dot.y = buf_y;
		dots.push_back(buf_dot);
	}
	buf_x = N, buf_y = 0;//4
	for (buf_y = 1; buf_y < N; buf_y++)
	{
		Dot buf_dot;
		buf_dot.Border = bord;
		buf_dot.x = buf_x;
		buf_dot.y = buf_y;
		dots.push_back(buf_dot);
	}
	////////////////////////////

	if (radio)
	{
		//Native
		for (buf_x = 1; buf_x < N; buf_x++)
		{
			for (buf_y = 1; buf_y < N; buf_y++)
			{
				Dot buf_dot;
				buf_dot.Border = false;
				buf_dot.x = buf_x;
				buf_dot.y = buf_y;
				dots.push_back(buf_dot);
			}
		}		
		if (Check_vakansii.GetCheck() == 1)
		{
			for (int i = 0; i < dots.size(); i++) //Создание вакансий
			{
				double rand_delete = 0 + 1000. * rand() / RAND_MAX;
				if (dots[i].Border) continue; //не удалять граничные атомы
				if (rand_delete < 250)
				{
					dots.erase(dots.begin() + i);
					i--;
				}
			}
			for (int i = 0; i < dots.size(); i++) // смещение центров остальных атомов
			{
				if (dots[i].Border) continue;//не сдвигать граничные атомы
				double c = 0.1;
				double r = 0. + 1. * rand() / RAND_MAX;
				dots[i].x += c * (-1. + 2. * r);
				r = 0. + 1. * rand() / RAND_MAX;
				dots[i].y += c * (-1. + 2. * r);
			}
		}
	}
	else
	{
		//Random
		int i_count;
		int iter;
		iter = 0;//кол-во итераций на добавление одной точки
		i_count = dots_count; //кол-во точек, которые необходимо добавить
		for (;;)
		{
			if (iter > 10000 || i_count == 0) break;
			double buf_xd, buf_yd;
			buf_xd = 1. + ((double)N - 2.) * rand() * 1. / RAND_MAX;
			buf_yd = 1. + ((double)N - 2.) * rand() * 1. / RAND_MAX;
			int ii;
			for (ii = 0; ii < dots.size(); ii++)
			{
				double line = sqrt((dots[ii].x - buf_xd) * (dots[ii].x - buf_xd) + (dots[ii].y - buf_yd) * (dots[ii].y - buf_yd));
				if (line < 0.85) break;
			}
			//
			if (ii == dots.size())
			{
				Dot buf_dot;
				buf_dot.Border = false;
				buf_dot.x = buf_xd;
				buf_dot.y = buf_yd;
				dots.push_back(buf_dot);
				iter = 0;
				i_count--;
			}
			else iter++;
		}
	}
	for (int i = 0; i < dots.size(); i++)
	{
		dots[i].x *= r_0;
		dots[i].y *= r_0;
	}
	dots_count_real = dots.size();
	gdiPaintDots();
	if (radio && Check_vakansii.GetCheck() == 0) par_korr_func(func_korr_abs,1);
	par_korr_func(func_korr,3);
	bt_START_STOP.CButton::EnableWindow(TRUE);
	UpdateData(0);
	SetCursor(LoadCursor(nullptr, IDC_ARROW));
}

void CExperimentoneDlg::OnBnClickedButton2()
{
	if (!timerOnOff)
	{
		OnBnClickedButton1();
		UpdateData(1);
		time = 0;
		dtau = tau * 2 * pow(10, -12);
		UpdateData(0);
		for (int i = 0; i < dots.size(); i++)
		{
			if (dots[i].Border) continue;
			dots[i].F_x = 0;
			dots[i].F_y = 0;
			for (int k = 0; k < dots.size(); k++)
			{
				if (k == i)continue;
				double kof = koeff_K(dots[i], dots[k]);
				if (kof == 0.)continue;
				dots[i].F_x += F_ik(dots, i, k, 0) * kof;
				dots[i].F_y += F_ik(dots, i, k, 1) * kof;
			}
		}
		mV_2usr = 0;
		E_kin.clear();
		E_pot.clear();
		E_full.clear();
		E_full_func();
		ViewerDraw();
		//////////////
		time_en = 0;
		en_buffer = 0;
		en_buffer_local = 0;
		//////////////
		SetTimer(1, 1, NULL);
		bt_START_STOP.SetWindowText((LPCWSTR)L"Стоп");
		timerOnOff = true;
	}
	else
	{
		KillTimer(1);
		bt_START_STOP.SetWindowText((LPCWSTR)L"Пуск");
		timerOnOff = false;
	}
}
void CExperimentoneDlg::OnTimer(UINT_PTR nIDEvent)
{
	UpdateData(1);
	iteration();
	gdiPaintDots();
	UpdateData(0);
	CDialogEx::OnTimer(nIDEvent);
}
void CExperimentoneDlg::iteration()
{
	time++;
	for (int i = 0; i < dots.size(); i++)
	{
		if (dots[i].Border) continue;
		dots[i].x += dots[i].v_x * dtau + (dots[i].F_x / (2 * m_argon)) * dtau * dtau;
		dots[i].y += dots[i].v_y * dtau + (dots[i].F_y / (2 * m_argon)) * dtau * dtau;
		double buf_F_x = dots[i].F_x;
		double buf_F_y = dots[i].F_y;
		dots[i].F_x = 0;
		dots[i].F_y = 0;
		for (int k = 0; k < dots.size(); k++)
		{
			if (k == i)continue;
			double kof = koeff_K(dots[i], dots[k]);
			if (kof == 0.)continue;
			dots[i].F_x += F_ik(dots, i, k, 0) * kof;
			dots[i].F_y += F_ik(dots, i, k, 1) * kof;
		}
		dots[i].v_x += ((dots[i].F_x + buf_F_x) / (2 * m_argon)) * dtau;
		dots[i].v_y += ((dots[i].F_y + buf_F_y) / (2 * m_argon)) * dtau;
	}
	if (tempOn && (time < 2000))
	{
		for (int i = 0; i < dots.size(); i++)
		{
			double V = sqrt(dots[i].v_x * dots[i].v_x + dots[i].v_y * dots[i].v_y);
			mV_2usr += m_argon * V * V;
		}
		if ((time % 10) == 0)
		{
			mV_2usr /= 10;
			double betta = koeff_B(mV_2usr);
			for (int i = 0; i < dots.size(); i++)
			{
				dots[i].v_x *= betta;
				dots[i].v_y *= betta;
			}
			mV_2usr = 0;
		}
	}
	par_korr_func(func_korr, 3);
	E_full_func();
	time_en++;
	en_buffer+=E_full[E_full.size()-1];
	if (time_en == 50)
	{
		time_en = 0;
		double proc = en_buffer / 50;
		en_buffer = 0;
	}
	ViewerDraw();
	if (time>=2000 && tmr_kill)
	{
		KillTimer(1);
		bt_START_STOP.SetWindowText((LPCWSTR)L"Пуск");
		timerOnOff = false;
	}
}

void CExperimentoneDlg::OnBnClickedRadio1() //Native
{
	radio = true;
}


void CExperimentoneDlg::OnBnClickedRadio2() //Random
{
	radio = false;
}

double CExperimentoneDlg::F_ik(vector<Dot>dots, int i, int k, int j)
{
	if (j == 0) return 12 * D * pow(r_0, 6) * \
		(pow(r_0, 6) / pow((pow((dots[i].x - dots[k].x), 2) + pow((dots[i].y - dots[k].y), 2)), 3) - 1)\
		* (dots[i].x - dots[k].x) / pow((pow((dots[i].x - dots[k].x), 2) + pow((dots[i].y - dots[k].y), 2)), 4);
	if (j == 1) return 12 * D * pow(r_0, 6) * \
		(pow(r_0, 6) / pow((pow((dots[i].x - dots[k].x), 2) + pow((dots[i].y - dots[k].y), 2)), 3) - 1)\
		* (dots[i].y - dots[k].y) / pow((pow((dots[i].x - dots[k].x), 2) + pow((dots[i].y - dots[k].y), 2)), 4);
}
double CExperimentoneDlg::koeff_K(Dot d1, Dot d2)
{
	double r1 = 1.1 * r_0;
	double r2 = 1.7 * r_0;
	double r = sqrt(pow((d1.x - d2.x), 2) + pow((d1.y - d2.y), 2));
	if (r < r1) return 1.;
	if (r >= r1 && r <= r2) return pow((1 - pow(((r - r1) / (r1 - r2)), 2)), 2);
	if (r > r2) return 0.;
}
void CExperimentoneDlg::gdiPaintDots()
{
	vector<double> dots_x, dots_y;
	for (int i = 0; i < dots.size(); i++)
	{
		dots_x.push_back(dots[i].x);
		dots_y.push_back(dots[i].y);

	}
	gdiObj.DotsToRound(dots_x, dots_y);
	gdiObj.MyPaint(N);
}

void CExperimentoneDlg::E_kin_func()
{
	if (E_kin.size() > 2000) E_kin.erase(E_kin.begin());
	double buffer = 0;
	for (int i = 0; i < dots.size(); i++)
	{
		double buf_v = dots[i].v_x * dots[i].v_x + dots[i].v_y * dots[i].v_y;
		buffer += m_argon * buf_v / 2;
	}
	E_kin.push_back(buffer);
}

void CExperimentoneDlg::E_pot_func()
{
	if (E_pot.size() > 2000) E_pot.erase(E_pot.begin());
	double buffer = 0;
	for (int i = 0; i < dots.size(); i++)
	{
		for (int k = 0; k < dots.size(); k++)
		{
			if (k == i)continue;
			double kof = koeff_K(dots[i], dots[k]);
			if (kof == 0.)continue;
			double r = sqrt(pow((dots[i].x - dots[k].x), 2) + pow((dots[i].y - dots[k].y), 2));
			r = r_0 / r;
			buffer += -D * (pow(r, 12) - 2 * pow(r, 6));
		}
	}
	E_pot.push_back(buffer);
}

void CExperimentoneDlg::E_full_func()
{
	E_kin_func();
	E_pot_func();
	if (E_full.size() > 2000) E_full.erase(E_full.begin());
	int num_kek = E_kin.size() - 1;
	E_kin[num_kek] /= 1.6 * pow(10, -19);
	E_pot[num_kek] /= 1.6 * pow(10, -19);
	double buffer = (E_kin[num_kek] + E_pot[num_kek]);
	E_full.push_back(buffer);
}
double CExperimentoneDlg::koeff_B(double& buf)
{
	double ret = 2 * dots.size() * k_b * Temp;
	ret /= buf;
	ret = sqrt(ret);
	temp_real = Temp / ret;
	return ret;
}
void CExperimentoneDlg::par_korr_func(vector<double>& func,int kk)
{
	func.clear(); func.resize(50);
	for (int i = 0; i < dots.size(); i++)
	{
		double left, right, up, down;
		down = left = N * r_0 / 4;
		up = right = 3 * N * r_0 / 4;
		if (dots[i].x < left || dots[i].x > right || dots[i].y < down || dots[i].y > up) continue;
		for (int j = 0; j < dots.size(); j++)
		{
			if (i == j) continue;
			double rrr = sqrt((dots[i].x - dots[j].x) * (dots[i].x - dots[j].x) + (dots[i].y - dots[j].y) * (dots[i].y - dots[j].y));
			if (rrr > 5 * r_0)continue;
			double step = 5 * r_0 / func.size();
			rrr /= step;
			int buf_num = GetBliz(rrr);
			if (buf_num > (func.size()-1))continue;
			func[GetBliz(rrr)] += 1.*kk;
		}
	}
	viewer_korr_Draw();
}
int CExperimentoneDlg::GetBliz(double x) //получение ближайшего целого числа
{
	int k = ceil(x);
	double mod1, mod2;
	mod1 = abs(x - k); mod2 = abs(x - (k - 1));
	if (mod1 < mod2) return k;
	else return(k - 1);
}
DoubleArray CExperimentoneDlg::vectorToArray(std::vector<double>& v)
{
	return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
}
void CExperimentoneDlg::ViewerDraw()
{
	// In this example, we simply use random data for the 3 data series.
	DoubleArray dataEKin = vectorToArray(E_kin);
	DoubleArray dataEpot = vectorToArray(E_pot);
	DoubleArray dataEfull = vectorToArray(E_full);
	vector<double>Datatime;
	for (int i = 0; i < E_kin.size(); i++)Datatime.push_back(i);
	DoubleArray timeStamps = vectorToArray(Datatime);

	// Create a XYChart object of size 600 x 400 pixels
	XYChart* c = new XYChart(780, 300);

	// Add a title box using grey (0x555555) 20pt Arial font
	c->addTitle("Energy", "arial.ttf", 20, 0x555555);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	c->setPlotArea(100, 80, 580, 150, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox* b = c->addLegend(70, 35, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	// Set axis label font to 12pt Arial
	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	c->xAxis()->setColors(Chart::TextColor, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::TextColor);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	c->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	c->xAxis()->setTickDensity(80);
	c->yAxis()->setTickDensity(40);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	c->yAxis()->setTitle("", "arial.ttf", 14, 0x555555);
	// Add a line layer to the chart with 3-pixel line width
	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);

	// Add 3 data series to the line layer
	layer->addDataSet(dataEKin, 0x5588cc, "E_kin");
	layer->addDataSet(dataEpot, 0xee9944, "E_pot");
	layer->addDataSet(dataEfull, 0x99bb55, "E_full");

	// The x-coordinates for the line layer
	layer->setXData(timeStamps);
	viewer.setChart(c);
	delete c;
}
void CExperimentoneDlg::viewer_korr_Draw()
{
	// In this example, we simply use random data for the 3 data series.
	DoubleArray datafunc_korr = vectorToArray(func_korr);
	DoubleArray datafunc_korr_abs = vectorToArray(func_korr_abs);
	vector<double>Datatime;
	for (int i = 0; i < 30; i++)Datatime.push_back(i);
	DoubleArray timeStamps = vectorToArray(Datatime);

	// Create a XYChart object of size 600 x 400 pixels
	XYChart* c = new XYChart(780, 300);

	// Add a title box using grey (0x555555) 20pt Arial font
	c->addTitle("korr", "arial.ttf", 20, 0x555555);

	// Set the plotarea at (70, 70) and of size 500 x 300 pixels, with transparent background and
	// border and light grey (0xcccccc) horizontal grid lines
	c->setPlotArea(0, 80, 750, 150, Chart::Transparent, -1, Chart::Transparent, 0xcccccc);

	// Add a legend box with horizontal layout above the plot area at (70, 35). Use 12pt Arial font,
	// transparent background and border, and line style legend icon.
	LegendBox* b = c->addLegend(70, 35, false, "arial.ttf", 12);
	b->setBackground(Chart::Transparent, Chart::Transparent);
	b->setLineStyleKey();

	// Set axis label font to 12pt Arial
	c->xAxis()->setLabelStyle("arial.ttf", 12);
	c->yAxis()->setLabelStyle("arial.ttf", 12);

	// Set the x and y axis stems to transparent, and the x-axis tick color to grey (0xaaaaaa)
	c->xAxis()->setColors(Chart::TextColor, Chart::TextColor, Chart::TextColor, 0xaaaaaa);
	c->yAxis()->setColors(Chart::TextColor);

	// Set the major/minor tick lengths for the x-axis to 10 and 0.
	c->xAxis()->setTickLength(10, 0);

	// For the automatic axis labels, set the minimum spacing to 80/40 pixels for the x/y axis.
	c->xAxis()->setTickDensity(80);
	c->yAxis()->setTickDensity(40);

	// Add a title to the y axis using dark grey (0x555555) 14pt Arial font
	c->yAxis()->setTitle("", "arial.ttf", 14, 0x555555);

	// Add a line layer to the chart with 3-pixel line width
	LineLayer* layer = c->addLineLayer();
	layer->setLineWidth(3);

	// Add 3 data series to the line layer
	layer->addDataSet(datafunc_korr, 0x5588cc, "korr");
	layer->addDataSet(datafunc_korr_abs, 0xee9944, "korr_abs");

	// The x-coordinates for the line layer
	layer->setXData(timeStamps);
	viewer_korr.setChart(c);
	delete c;
}
